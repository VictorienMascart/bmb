﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileType : Form
	{
		public Devis Devis;
		public Facture Facture;
		public DevisFacileAccueil DevisFacileAccueil;

		public DevisFacileType(DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		public DevisFacileType(DevisFacileAccueil devisFacileAccueil, Facture facture)
		{
			Facture = facture;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		public DevisFacileType(DevisFacileAccueil devisFacileAccueil, Devis devis)
		{
			Devis = devis;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}



		public DevisFacileType()
		{
			InitializeComponent();
		}

		private void DevisFacileType_Load(object sender, EventArgs e)
		{
			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");

		}

		private void btnProfessionnel_Click(object sender, EventArgs e)
		{


			MODEL.CLASSE.TypeD typedevis = new MODEL.CLASSE.TypeD(2, "Professionnel");

			

			if(Facture != null)
			{
				Facture.TypeD = typedevis;
				Facture.IndiceFacture = "FC";

				DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(Facture, DevisFacileAccueil);
				DevisFacileChoixTVA.Show();
				Close();
			}
			else
			{
				Devis.TypeD = typedevis;

				DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(Devis, DevisFacileAccueil);
				DevisFacileChoixTVA.Show();
				Close();
			}

			if(Facture != null)
			{
				labelTitre.Text = "Type de Facture";
			}
			
		}

		private void btnParticulier_Click(object sender, EventArgs e)
		{
			MODEL.CLASSE.TypeD typedevis = new MODEL.CLASSE.TypeD(3, "Particulier");

			if (Facture != null)
			{
				Facture.TypeD = typedevis;

				DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(Facture, DevisFacileAccueil);
				DevisFacileChoixTVA.Show();
				this.Close();
			}
			else
			{
			
				Devis.TypeD = typedevis;

				DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(Devis, DevisFacileAccueil);
				DevisFacileChoixTVA.Show();
				this.Close();
			}
		}

		private void btnRetour_Click(object sender, EventArgs e)
		{
			DevisFacileAccueil.WindowState = FormWindowState.Normal;
			this.Close();
		}
	}
}
