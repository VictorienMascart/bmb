﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileCreationFacture
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureRetour = new System.Windows.Forms.PictureBox();
			this.txtDescriptionTravaux = new System.Windows.Forms.TextBox();
			this.lbdescription = new System.Windows.Forms.Label();
			this.txtDate = new System.Windows.Forms.TextBox();
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.checkBoxOption = new System.Windows.Forms.CheckBox();
			this.textBoxTTC = new System.Windows.Forms.TextBox();
			this.textBoxTVA = new System.Windows.Forms.TextBox();
			this.textHorsTaxes = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lbAdresse = new System.Windows.Forms.Label();
			this.lbIdentifiant = new System.Windows.Forms.Label();
			this.txtAdresseClient = new System.Windows.Forms.TextBox();
			this.txtNomClient = new System.Windows.Forms.TextBox();
			this.lbClient = new System.Windows.Forms.Label();
			this.Picture = new System.Windows.Forms.PictureBox();
			this.btnPDF = new System.Windows.Forms.Button();
			this.lbNumero = new System.Windows.Forms.Label();
			this.btnRetour = new System.Windows.Forms.Button();
			this.btnSelectClient = new System.Windows.Forms.Button();
			this.btnLibelle = new System.Windows.Forms.Button();
			this.lbPrixFacture = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.Libellé = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Unité = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Quantité = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Pxuniteuros = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Totaleuros = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Option = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Supprimer = new System.Windows.Forms.DataGridViewButtonColumn();
			this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.pictureRetour)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureRetour
			// 
			this.pictureRetour.Location = new System.Drawing.Point(-174, -182);
			this.pictureRetour.Name = "pictureRetour";
			this.pictureRetour.Size = new System.Drawing.Size(40, 19);
			this.pictureRetour.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureRetour.TabIndex = 76;
			this.pictureRetour.TabStop = false;
			// 
			// txtDescriptionTravaux
			// 
			this.txtDescriptionTravaux.Location = new System.Drawing.Point(382, 192);
			this.txtDescriptionTravaux.Name = "txtDescriptionTravaux";
			this.txtDescriptionTravaux.Size = new System.Drawing.Size(593, 20);
			this.txtDescriptionTravaux.TabIndex = 75;
			// 
			// lbdescription
			// 
			this.lbdescription.AutoSize = true;
			this.lbdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbdescription.Location = new System.Drawing.Point(225, 195);
			this.lbdescription.Name = "lbdescription";
			this.lbdescription.Size = new System.Drawing.Size(155, 13);
			this.lbdescription.TabIndex = 74;
			this.lbdescription.Text = "Descriptions des travaux :";
			// 
			// txtDate
			// 
			this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtDate.Location = new System.Drawing.Point(240, 165);
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(101, 13);
			this.txtDate.TabIndex = 73;
			this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dataGridView
			// 
			this.dataGridView.BackgroundColor = System.Drawing.Color.Silver;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Libellé,
            this.Unité,
            this.Quantité,
            this.Pxuniteuros,
            this.Totaleuros,
            this.Option,
            this.Supprimer,
            this.id});
			this.dataGridView.Location = new System.Drawing.Point(12, 219);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.Size = new System.Drawing.Size(963, 468);
			this.dataGridView.TabIndex = 70;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellContentClick_1);
			this.dataGridView.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellLeave_1);
			this.dataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellValueChanged_1);
			this.dataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DataGridView_RowsAdded_1);
			// 
			// checkBoxOption
			// 
			this.checkBoxOption.AutoSize = true;
			this.checkBoxOption.Location = new System.Drawing.Point(12, 693);
			this.checkBoxOption.Name = "checkBoxOption";
			this.checkBoxOption.Size = new System.Drawing.Size(111, 17);
			this.checkBoxOption.TabIndex = 63;
			this.checkBoxOption.Text = "Valider les options";
			this.checkBoxOption.UseVisualStyleBackColor = true;
			this.checkBoxOption.CheckedChanged += new System.EventHandler(this.CheckBoxOption_CheckedChanged);
			// 
			// textBoxTTC
			// 
			this.textBoxTTC.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxTTC.Location = new System.Drawing.Point(253, 738);
			this.textBoxTTC.Name = "textBoxTTC";
			this.textBoxTTC.Size = new System.Drawing.Size(100, 13);
			this.textBoxTTC.TabIndex = 62;
			this.textBoxTTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxTVA
			// 
			this.textBoxTVA.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxTVA.Location = new System.Drawing.Point(131, 738);
			this.textBoxTVA.Name = "textBoxTVA";
			this.textBoxTVA.Size = new System.Drawing.Size(100, 13);
			this.textBoxTVA.TabIndex = 61;
			this.textBoxTVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textHorsTaxes
			// 
			this.textHorsTaxes.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textHorsTaxes.Location = new System.Drawing.Point(12, 738);
			this.textHorsTaxes.Name = "textHorsTaxes";
			this.textHorsTaxes.Size = new System.Drawing.Size(100, 13);
			this.textHorsTaxes.TabIndex = 60;
			this.textHorsTaxes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textHorsTaxes.TextChanged += new System.EventHandler(this.TextHorsTaxes_TextChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(282, 722);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 13);
			this.label5.TabIndex = 59;
			this.label5.Text = "TTC  €";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(162, 722);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 13);
			this.label4.TabIndex = 58;
			this.label4.Text = "TVA  €";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(15, 722);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(97, 13);
			this.label3.TabIndex = 57;
			this.label3.Text = "Base Hors Taxes €";
			// 
			// lbAdresse
			// 
			this.lbAdresse.AutoSize = true;
			this.lbAdresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbAdresse.Location = new System.Drawing.Point(306, 92);
			this.lbAdresse.Name = "lbAdresse";
			this.lbAdresse.Size = new System.Drawing.Size(51, 13);
			this.lbAdresse.TabIndex = 56;
			this.lbAdresse.Text = "Adresse :";
			// 
			// lbIdentifiant
			// 
			this.lbIdentifiant.AutoSize = true;
			this.lbIdentifiant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbIdentifiant.Location = new System.Drawing.Point(306, 66);
			this.lbIdentifiant.Name = "lbIdentifiant";
			this.lbIdentifiant.Size = new System.Drawing.Size(35, 13);
			this.lbIdentifiant.TabIndex = 55;
			this.lbIdentifiant.Text = "Nom :";
			// 
			// txtAdresseClient
			// 
			this.txtAdresseClient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtAdresseClient.Location = new System.Drawing.Point(359, 89);
			this.txtAdresseClient.Multiline = true;
			this.txtAdresseClient.Name = "txtAdresseClient";
			this.txtAdresseClient.Size = new System.Drawing.Size(236, 60);
			this.txtAdresseClient.TabIndex = 54;
			// 
			// txtNomClient
			// 
			this.txtNomClient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNomClient.Location = new System.Drawing.Point(359, 63);
			this.txtNomClient.Name = "txtNomClient";
			this.txtNomClient.Size = new System.Drawing.Size(236, 20);
			this.txtNomClient.TabIndex = 53;
			// 
			// lbClient
			// 
			this.lbClient.AutoSize = true;
			this.lbClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbClient.Location = new System.Drawing.Point(281, 37);
			this.lbClient.Name = "lbClient";
			this.lbClient.Size = new System.Drawing.Size(47, 13);
			this.lbClient.TabIndex = 52;
			this.lbClient.Text = "Client :";
			// 
			// Picture
			// 
			this.Picture.Location = new System.Drawing.Point(22, 60);
			this.Picture.Name = "Picture";
			this.Picture.Size = new System.Drawing.Size(165, 89);
			this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Picture.TabIndex = 51;
			this.Picture.TabStop = false;
			// 
			// btnPDF
			// 
			this.btnPDF.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnPDF.Location = new System.Drawing.Point(857, 702);
			this.btnPDF.Name = "btnPDF";
			this.btnPDF.Size = new System.Drawing.Size(118, 52);
			this.btnPDF.TabIndex = 50;
			this.btnPDF.Text = "éditer la facture";
			this.btnPDF.UseVisualStyleBackColor = true;
			this.btnPDF.Click += new System.EventHandler(this.BtnPDF_Click);
			// 
			// lbNumero
			// 
			this.lbNumero.AutoSize = true;
			this.lbNumero.Location = new System.Drawing.Point(42, 165);
			this.lbNumero.Name = "lbNumero";
			this.lbNumero.Size = new System.Drawing.Size(70, 13);
			this.lbNumero.TabIndex = 48;
			this.lbNumero.Text = "LabelNumero";
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.Location = new System.Drawing.Point(12, 12);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 78;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.BtnRetour_Click);
			// 
			// btnSelectClient
			// 
			this.btnSelectClient.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnSelectClient.Location = new System.Drawing.Point(334, 32);
			this.btnSelectClient.Name = "btnSelectClient";
			this.btnSelectClient.Size = new System.Drawing.Size(125, 23);
			this.btnSelectClient.TabIndex = 79;
			this.btnSelectClient.Text = "Selectionner un client existant";
			this.btnSelectClient.UseVisualStyleBackColor = true;
			this.btnSelectClient.Click += new System.EventHandler(this.BtnSelectClient_Click);
			// 
			// btnLibelle
			// 
			this.btnLibelle.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnLibelle.Location = new System.Drawing.Point(12, 190);
			this.btnLibelle.Name = "btnLibelle";
			this.btnLibelle.Size = new System.Drawing.Size(140, 23);
			this.btnLibelle.TabIndex = 80;
			this.btnLibelle.Text = "ajouter un libellé existant";
			this.btnLibelle.UseVisualStyleBackColor = true;
			this.btnLibelle.Click += new System.EventHandler(this.BtnLibelle_Click);
			// 
			// lbPrixFacture
			// 
			this.lbPrixFacture.AutoSize = true;
			this.lbPrixFacture.Location = new System.Drawing.Point(379, 722);
			this.lbPrixFacture.Name = "lbPrixFacture";
			this.lbPrixFacture.Size = new System.Drawing.Size(46, 13);
			this.lbPrixFacture.TabIndex = 81;
			this.lbPrixFacture.Text = "lbprixfac";
			// 
			// textBox1
			// 
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Location = new System.Drawing.Point(359, 738);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 13);
			this.textBox1.TabIndex = 82;
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Libellé
			// 
			this.Libellé.HeaderText = "Libellé";
			this.Libellé.Name = "Libellé";
			this.Libellé.Width = 320;
			// 
			// Unité
			// 
			this.Unité.HeaderText = "Unité";
			this.Unité.Items.AddRange(new object[] {
            "U",
            "m2",
            "m3",
            "mm",
            "cm",
            "m",
            "L",
            "ml",
            "Kg",
            "T"});
			this.Unité.Name = "Unité";
			this.Unité.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Unité.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// Quantité
			// 
			this.Quantité.HeaderText = "Quantité";
			this.Quantité.Name = "Quantité";
			// 
			// Pxuniteuros
			// 
			this.Pxuniteuros.HeaderText = "Px unit. euros";
			this.Pxuniteuros.Name = "Pxuniteuros";
			// 
			// Totaleuros
			// 
			this.Totaleuros.HeaderText = "Total euros";
			this.Totaleuros.Name = "Totaleuros";
			this.Totaleuros.ReadOnly = true;
			// 
			// Option
			// 
			this.Option.HeaderText = "Option";
			this.Option.Name = "Option";
			// 
			// Supprimer
			// 
			this.Supprimer.HeaderText = "";
			this.Supprimer.Name = "Supprimer";
			this.Supprimer.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Supprimer.Text = "Supprimer";
			this.Supprimer.UseColumnTextForButtonValue = true;
			// 
			// id
			// 
			this.id.HeaderText = "id";
			this.id.Name = "id";
			this.id.ReadOnly = true;
			this.id.Visible = false;
			// 
			// DevisFacileCreationFacture
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(988, 759);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.lbPrixFacture);
			this.Controls.Add(this.btnLibelle);
			this.Controls.Add(this.btnSelectClient);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.pictureRetour);
			this.Controls.Add(this.txtDescriptionTravaux);
			this.Controls.Add(this.lbdescription);
			this.Controls.Add(this.txtDate);
			this.Controls.Add(this.dataGridView);
			this.Controls.Add(this.checkBoxOption);
			this.Controls.Add(this.textBoxTTC);
			this.Controls.Add(this.textBoxTVA);
			this.Controls.Add(this.textHorsTaxes);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lbAdresse);
			this.Controls.Add(this.lbIdentifiant);
			this.Controls.Add(this.txtAdresseClient);
			this.Controls.Add(this.txtNomClient);
			this.Controls.Add(this.lbClient);
			this.Controls.Add(this.Picture);
			this.Controls.Add(this.btnPDF);
			this.Controls.Add(this.lbNumero);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileCreationFacture";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DevisFacileCreationFacture";
			this.Load += new System.EventHandler(this.DevisFacileCreationFacture_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureRetour)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureRetour;
		private System.Windows.Forms.TextBox txtDescriptionTravaux;
		private System.Windows.Forms.Label lbdescription;
		private System.Windows.Forms.TextBox txtDate;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.CheckBox checkBoxOption;
		private System.Windows.Forms.TextBox textBoxTTC;
		private System.Windows.Forms.TextBox textBoxTVA;
		private System.Windows.Forms.TextBox textHorsTaxes;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lbAdresse;
		private System.Windows.Forms.Label lbIdentifiant;
		private System.Windows.Forms.TextBox txtAdresseClient;
		private System.Windows.Forms.TextBox txtNomClient;
		private System.Windows.Forms.Label lbClient;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Button btnPDF;
		private System.Windows.Forms.Label lbNumero;
		private System.Windows.Forms.Button btnRetour;
		private System.Windows.Forms.Button btnSelectClient;
		private System.Windows.Forms.Button btnLibelle;
		private System.Windows.Forms.Label lbPrixFacture;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Libellé;
		private System.Windows.Forms.DataGridViewComboBoxColumn Unité;
		private System.Windows.Forms.DataGridViewTextBoxColumn Quantité;
		private System.Windows.Forms.DataGridViewTextBoxColumn Pxuniteuros;
		private System.Windows.Forms.DataGridViewTextBoxColumn Totaleuros;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Option;
		private System.Windows.Forms.DataGridViewButtonColumn Supprimer;
		private System.Windows.Forms.DataGridViewTextBoxColumn id;
	}
}