﻿namespace DevisFacile.VIEW
{
    partial class DevisFacileListAvoir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listAvoirs = new System.Windows.Forms.ListBox();
            this.btnGenereravoir = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listAvoirs
            // 
            this.listAvoirs.FormattingEnabled = true;
            this.listAvoirs.Location = new System.Drawing.Point(12, 12);
            this.listAvoirs.Name = "listAvoirs";
            this.listAvoirs.Size = new System.Drawing.Size(444, 433);
            this.listAvoirs.TabIndex = 0;
            // 
            // btnGenereravoir
            // 
            this.btnGenereravoir.Location = new System.Drawing.Point(531, 12);
            this.btnGenereravoir.Name = "btnGenereravoir";
            this.btnGenereravoir.Size = new System.Drawing.Size(75, 48);
            this.btnGenereravoir.TabIndex = 1;
            this.btnGenereravoir.Text = "générer les avoirs";
            this.btnGenereravoir.UseVisualStyleBackColor = true;
            this.btnGenereravoir.Click += new System.EventHandler(this.btnGenereravoir_Click_1);
            // 
            // btnSuppr
            // 
            this.btnSuppr.Location = new System.Drawing.Point(531, 78);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(75, 46);
            this.btnSuppr.TabIndex = 2;
            this.btnSuppr.Text = "Supprimer l\'avoir";
            this.btnSuppr.UseVisualStyleBackColor = true;
            this.btnSuppr.Click += new System.EventHandler(this.btnSuppr_Click);
            // 
            // btnRetour
            // 
            this.btnRetour.Location = new System.Drawing.Point(531, 409);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(75, 29);
            this.btnRetour.TabIndex = 3;
            this.btnRetour.Text = "retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // DevisFacileListAvoir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 450);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.btnGenereravoir);
            this.Controls.Add(this.listAvoirs);
            this.Name = "DevisFacileListAvoir";
            this.Text = "DevisFacileListAvoir";
            this.Load += new System.EventHandler(this.DevisFacileListAvoir_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listAvoirs;
        private System.Windows.Forms.Button btnGenereravoir;
        private System.Windows.Forms.Button btnSuppr;
        private System.Windows.Forms.Button btnRetour;
    }
}