﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileCreationFacture : Form
	{
		public FactureDAO FactureDAO = new FactureDAO();
		public DevisFacileAccueil DevisFacileAccueil;
		public Facture LaFacture;
		public ListAvoir DevisFacileListDevis;
		public DevisFacileListFacture DevisFacileListFacture;

		public string IndiceFacturation;

		public DevisFacileCreationFacture()
		{
			InitializeComponent();
		}

		public DevisFacileCreationFacture(Facture LaFacture, DevisFacileListFacture devisFacileListFacture, DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			DevisFacileListFacture = devisFacileListFacture;
			this.LaFacture = LaFacture;
			InitializeComponent();
		}

		public DevisFacileCreationFacture(Facture LaFacture, ListAvoir devisFacileListDevis, DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			DevisFacileListDevis = devisFacileListDevis;
			this.LaFacture = LaFacture;
			InitializeComponent();
		}

		public DevisFacileCreationFacture(Facture LaFacture, DevisFacileAccueil devisFacileAccueil, string indiceFacturation)
		{
			DevisFacileAccueil = devisFacileAccueil;
			this.LaFacture = LaFacture;
			IndiceFacturation = indiceFacturation;
			InitializeComponent();
		}

		public DevisFacileCreationFacture(Facture LaFacture, DevisFacileAccueil devisFacileAccueil, string indiceFacturation, ListAvoir devisFacileListDevis)
		{
			DevisFacileListDevis = devisFacileListDevis;
			DevisFacileAccueil = devisFacileAccueil;
			this.LaFacture = LaFacture;
			IndiceFacturation = indiceFacturation;
			InitializeComponent();
		}

		public void DataGridViewValue(Element element)
		{
			if (element != null)
			{
				decimal total = element.Prix_unit * element.Quantite;

				string[] row = { element.Libelle, element.Unite.ToString(), element.Quantite.ToString(), element.Prix_unit.ToString(), total.ToString(), element.IsOption.ToString(), "supprimer", element.Id.ToString() };

				dataGridView.Rows.Add(row);

			}

		}

		public string TxtNomClientValue
		{
			get
			{
				return this.txtNomClient.Text;
			}

			set
			{
				this.txtNomClient.Text = value;
			}
		}
		public string TxtAdresseClientValue
		{
			get
			{
				return this.txtAdresseClient.Text;
			}

			set
			{
				this.txtAdresseClient.Text = value;
			}
		}

		public bool IsNumeric(string Nombre)
		{
			try
			{
				double.Parse(Nombre);
				return true;
			}
			catch
			{
				return false;
			}
		}

		private void DevisFacileCreationFacture_Load(object sender, EventArgs e)
		{

			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");

			if (LaFacture != null)
			{
				string now = DateTime.Now.ToString();
				now = now.Substring(0, 10);
				txtDate.Text = now;
				textBoxTVA.Text = LaFacture.TVA.ToString();

				if (IndiceFacturation != null)
				{
					LaFacture.IndiceFacture = IndiceFacturation;
				}
				
				if (LaFacture.IndiceFacture == "FC")
				{
					lbPrixFacture.Visible = false;
					textBox1.Visible = false;
				}
				if (LaFacture.IndiceFacture == "FA")
				{
					lbPrixFacture.Text = " Coût (40% du ttc) : ";

				}
				else if (LaFacture.IndiceFacture == "FL")
				{
					lbPrixFacture.Text = " Coût (60% du ttc) : ";
				}

				if (LaFacture != null && (DevisFacileListFacture != null || DevisFacileListDevis != null))
				{

					if (LaFacture.Voption)
					{
						checkBoxOption.Checked = true;
					}

					txtAdresseClient.Text = LaFacture.Client.Adresse.ToString();
					txtNomClient.Text = LaFacture.Client.Nom.ToString();
					foreach (Element E in LaFacture.Elements)
					{
						DataGridViewValue(E);
					}
					txtDescriptionTravaux.Text = LaFacture.DescriptifTravaux;

					if (DevisFacileListFacture != null)
					{
						if(LaFacture.IndiceFacture == "FC")
							lbNumero.Text = "Facture n°: " + LaFacture.Numero;
						else if (LaFacture.IndiceFacture == "FA")
							lbNumero.Text = "Facture d'acompte n°" + LaFacture.Numero;
						else if (LaFacture.IndiceFacture == "FL")
							lbNumero.Text = "Bon de Livraison n°" + LaFacture.Numero;

						txtDate.Text = "du : " +LaFacture.DateTime.ToString().Substring(0, 10);
					}
					
					if(LaFacture.Voption == true)
					{
						checkBoxOption.Checked = true;
					}
				}

			}

			if (FactureDAO.GetLastNumeroFacture() != null)
			{
				string num = FactureDAO.GetLastNumeroFacture();

				string numero = num.Substring(num.Length - 3);

				int LastNumero = Convert.ToInt32(numero);

				LastNumero = LastNumero + 1;

				//afficher le numero
				if (LaFacture != null)
				{
					if (LaFacture.IndiceFacture == "FC")
						lbNumero.Text = "Facture n°" + "2015" + "/" + LastNumero;
					else if (LaFacture.IndiceFacture == "FA")
						lbNumero.Text = "Facture d'acompte n°" + "2015" + "/" + LastNumero;
					else if (LaFacture.IndiceFacture == "FL")
						lbNumero.Text = "Bon de Livraison n°" + "2015" + "/" + LastNumero;
				}
				else
				{
					if (IndiceFacturation == "FC")
						lbNumero.Text = "Facture n°" + "2015" + "/" + LastNumero;
					else if (IndiceFacturation == "FA")
						lbNumero.Text = "Facture d'acompte n°" + "2015" + "/" + LastNumero;
					else if (IndiceFacturation == "FL")
						lbNumero.Text = "Bon de Livraison n°" + "2015" + "/" + LastNumero;
				}
					
			}


		}

		private void BtnSelectClient_Click(object sender, EventArgs e)
		{
			DevisFacileListeClients devisFacileListeClients = new DevisFacileListeClients(LaFacture, this);
			devisFacileListeClients.ShowDialog();
		}

		private void BtnLibelle_Click(object sender, EventArgs e)
		{
			DevisFacileListeElements devisFacileListeElements = new DevisFacileListeElements(this);
			devisFacileListeElements.ShowDialog();
		}

		private void BtnRetour_Click(object sender, EventArgs e)
		{
			if (LaFacture != null && DevisFacileListFacture != null)
			{
				DevisFacileListFacture devisFacileListFacture = new DevisFacileListFacture(DevisFacileAccueil);
				devisFacileListFacture.Show();
				Close();
			}
			else if (LaFacture != null && DevisFacileListDevis != null)
			{
				if (LaFacture.TypeD.Libelle == "Particulier")
				{
					DevisFacileTypeFacture DevisFacileTypeFacture = new DevisFacileTypeFacture(LaFacture, DevisFacileListDevis, DevisFacileAccueil);
					DevisFacileTypeFacture.Show();
					this.Close();
				}
				else if (LaFacture.TypeD.Libelle == "Professionnel")
				{
                    DevisFacileListAvoir devisFacileListDevis = new DevisFacileListAvoir(DevisFacileAccueil);
					devisFacileListDevis.Show();
					Close();
				}



			}
			else
			{
				if(LaFacture.TypeD.Libelle == "Particulier")
				{
					DevisFacileTypeFacture DevisFacileTypeFacture = new DevisFacileTypeFacture(LaFacture, DevisFacileAccueil);
					DevisFacileTypeFacture.Show();
					this.Close();
				}else if (LaFacture.TypeD.Libelle == "Professionnel")
				{
					DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(LaFacture, DevisFacileAccueil);
					DevisFacileChoixTVA.Show();
					this.Close();
				}
				
			}
		}


		private void DataGridView_CellLeave_1(object sender, DataGridViewCellEventArgs e)
		{
			if ((e.ColumnIndex == 2 || e.ColumnIndex == 3))
			{
				if (!IsNumeric((string)dataGridView.CurrentCell.Value))
				{
					dataGridView.CurrentCell.Value = null;
				}
			}

		}

		private void DataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == 6 && dataGridView.Rows.Count > 1 && !dataGridView.Rows[dataGridView.CurrentCell.RowIndex].IsNewRow)
			{
				dataGridView.Rows.Remove(dataGridView.CurrentRow);
			}
		}

		private void DataGridView_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
		{

			if (dataGridView.RowCount > 0)
			{
				if (dataGridView.CurrentCell != null && (e.ColumnIndex == 2 || e.ColumnIndex == 3))
				{
					int L = dataGridView.CurrentCell.RowIndex;

					if (dataGridView.Rows[L].Cells[2].Value != null && dataGridView.Rows[L].Cells[3].Value != null)
					{
						if (IsNumeric((string)dataGridView.Rows[L].Cells[2].Value) && IsNumeric((string)dataGridView.Rows[L].Cells[3].Value))
						{
							double value = Math.Round(Convert.ToDouble(dataGridView.Rows[L].Cells[2].Value) * Convert.ToDouble(dataGridView.Rows[L].Cells[3].Value), 2);
							dataGridView.Rows[L].Cells[4].Value = value;
						}
					}

					if (!IsNumeric((string)dataGridView.Rows[L].Cells[2].Value))
						dataGridView.Rows[L].Cells[2].Value = null;
					if (!IsNumeric((string)dataGridView.Rows[L].Cells[3].Value))
						dataGridView.Rows[L].Cells[3].Value = null;
				}

				//calcule du hors taxes et ttc
				if (e.ColumnIndex == 5 || e.ColumnIndex == 4)
				{
					double somme = 0;
					if (!checkBoxOption.Checked)
					{


						for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
						{
							bool check = false;
							string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

							if (strValue == "True")
							{
								check = true;
							}

							if (check == false)
								somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

						}

						textHorsTaxes.Text = somme.ToString();
					}
					else
					{
						for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
						{
							somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

							dataGridView.Rows[i].Cells[5].ReadOnly = true;
						}

						textHorsTaxes.Text = somme.ToString();
					}

					if (textBoxTVA.Text == "auto liquidation")
					{
						textBoxTTC.Text = textHorsTaxes.Text;
					}
					else
					{
						double ttc = 0;
						if (textBoxTVA.Text == "10%")
						{
							ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

						}
						else
						{
							ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
						}
						textBoxTTC.Text = ttc.ToString();

						if (IndiceFacturation == "FA")
							textBox1.Text = ((((double)40 / 100) * ttc)).ToString();
						else if (IndiceFacturation == "FL")
							textBox1.Text = ((((double)60 / 100) * ttc)).ToString();
					}
				}
			}
		}

		private void DataGridView_RowsAdded_1(object sender, DataGridViewRowsAddedEventArgs e)
		{

			for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
			{
				dataGridView.Rows[i].Cells[6].Value = "supprimer";
			}

			//calcule du hors taxes et ttc
			double somme = 0;
			if (!checkBoxOption.Checked)
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					if (dataGridView.Rows[i].Cells[5].Value != null)
					{
						bool check = false;
						string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

						if (strValue == "True")
						{
							check = true;
						}

						if (check == false)
						{
							somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);
						}
					}
					else
					{
						dataGridView.Rows[i].Cells[5].Value = false;
					}
				}
			}
			else
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					if (dataGridView.Rows[i].Cells[5].Value != null)
					{
						somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

						dataGridView.Rows[i].Cells[5].ReadOnly = true;
					}
					else
					{
						dataGridView.Rows[i].Cells[5].Value = false;
					}
				}
			}
			textHorsTaxes.Text = somme.ToString();

			if (textBoxTVA.Text == "auto liquidation")
			{
				textBoxTTC.Text = textHorsTaxes.Text;
			}
			else
			{
				double ttc = 0;
				if (textBoxTVA.Text == "10%")
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

				}
				else
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
				}
				textBoxTTC.Text = ttc.ToString();

				if (IndiceFacturation == "FA")
					textBox1.Text = ((((double)40 / 100) * ttc)).ToString();
				else if (IndiceFacturation == "FL")
					textBox1.Text = ((((double)60 / 100) * ttc)).ToString();
			}
		}

		private void CheckBoxOption_CheckedChanged(object sender, EventArgs e)
		{
			double somme = 0;

			if (checkBoxOption.Checked)
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);
					dataGridView.Rows[i].Cells[5].ReadOnly = true;
				}
				for (int i = 0; i < dataGridView.Rows.Count; i++)
					dataGridView.Rows[i].Cells[5].ReadOnly = true;
			}
			else
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					bool check = false;
					string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

					if (strValue == "True")
					{
						check = true;
					}
					if (check == false)
					{
						somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);
					}
				}

				for (int i = 0; i < dataGridView.Rows.Count; i++)
					dataGridView.Rows[i].Cells[5].ReadOnly = false;
			}

			textHorsTaxes.Text = somme.ToString();
		}

		private void TextHorsTaxes_TextChanged(object sender, EventArgs e)
		{
			//calcule du hors taxes et ttc
			if (textBoxTVA.Text == "auto liquidation")
			{
				textBoxTTC.Text = textHorsTaxes.Text;
			}
			else
			{
				double ttc = 0;
				if (textBoxTVA.Text == "10%")
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

				}
				else
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
				}
				textBoxTTC.Text = ttc.ToString();

				if(IndiceFacturation != null)
				{
					if (IndiceFacturation == "FA")
						textBox1.Text = ((((double)40 / 100) * ttc)).ToString();
					else if (IndiceFacturation == "FL")
						textBox1.Text = ((((double)60 / 100) * ttc)).ToString();
					else if (IndiceFacturation == "FC")
						textBox1.Text = ttc.ToString();
				}
				else if (LaFacture.IndiceFacture != null)
				{
					if (LaFacture.IndiceFacture == "FA")
						textBox1.Text = ((((double)40 / 100) * ttc)).ToString();
					else if (LaFacture.IndiceFacture == "FL")
						textBox1.Text = ((((double)60 / 100) * ttc)).ToString();
					else if (LaFacture.IndiceFacture == "FC")
						textBox1.Text = ttc.ToString();
				}
				
			}
		}

		private void BtnPDF_Click(object sender, EventArgs e)
		{
			Facture facture = new Facture();
			if (LaFacture.NumeroDevis != null)
			{
				facture.NumeroDevis = LaFacture.NumeroDevis;
			}

			//Le Numero
			string Num = "";
			if (LaFacture.IndiceFacture == "FC")
			{
				Num = lbNumero.Text.Substring(10);
			}
			if (LaFacture.IndiceFacture == "FA")
			{
				Num = lbNumero.Text.Substring(20);
			}
			if (LaFacture.IndiceFacture == "FL")
			{
				Num = lbNumero.Text.Substring(19);
			}

			facture.Numero = Num;

			//La Date
			DateTime dateTime = Convert.ToDateTime(txtDate.Text);
			facture.DateTime = dateTime;

			//Le Client
			if (txtNomClient.Text != "" && txtAdresseClient.Text != "" && txtNomClient.Text != null && txtAdresseClient.Text != null)
			{
				string NomClient = txtNomClient.Text;
				string AdresseClient = txtAdresseClient.Text;

				Client client = new Client(NomClient, AdresseClient);
				ClientDAO clientDAO = new ClientDAO();
				if (clientDAO.IfExistLeClient(client))
					client = clientDAO.SelectIdClient(client);
				else
				{
					clientDAO.InsertLeClient(client);
					client = clientDAO.SelectIdClient(client);
				}
				if (client != null)
				{
					if (client.Id != 0 && (client.Nom != "" && client.Nom != null) && (client.Adresse != "" && client.Adresse != null))
					{
						facture.Client = new Client();
						facture.Client = client;
					}
				}
				
			}

			//La Description
			string desc = txtDescriptionTravaux.Text;
			facture.DescriptifTravaux = desc;

			//Les éléments
			List<Element> LesElements = new List<Element>();

			for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
			{
				Element element = new Element();
				ElementDAO elementDAO = new ElementDAO();

				element.Id = Convert.ToInt32(dataGridView.Rows[i].Cells[7].Value);
				element.Libelle = (string)dataGridView.Rows[i].Cells[0].Value;

				//l'UNITE

				Unite unite = new Unite();
				UniteDAO uniteDAO = new UniteDAO();

				if (dataGridView.Rows[i].Cells[1].Value != null)
				{
					unite.Libelle = dataGridView.Rows[i].Cells[1].Value.ToString();

					unite = uniteDAO.SelectChampsUnite(unite);

					element.Unite = unite;
				}

				



				bool quantiteVerif = decimal.TryParse((string)dataGridView.Rows[i].Cells[2].Value, out decimal quantite);
				element.Quantite = quantite;
				bool prixUnitVerif = decimal.TryParse((string)dataGridView.Rows[i].Cells[3].Value, out decimal prixU);
				element.Prix_unit = prixU;

				//IsOption

				element.IsOption = false;

				if (dataGridView.Rows[i].Cells[5].Value.GetType().Equals(typeof(string)))
				{
					string val = dataGridView.Rows[i].Cells[5].Value as string;

					if (val == "True")
					{
						element.IsOption = true;
					}

				}
				else if (dataGridView.Rows[i].Cells[5].Value.GetType().Equals(typeof(bool)))
				{
					bool val = (bool)dataGridView.Rows[i].Cells[5].Value;

					if (val == true)
					{
						element.IsOption = true;
					}
				}

				

				


				if (quantiteVerif == true && prixUnitVerif == true && unite != null && element.Unite != null && element.Libelle != null && (unite.Libelle != "" && element.Libelle != ""))
				{

					if (elementDAO.IfExistLeElement(element) == false)
					{
						elementDAO.InsertLeElement(element);
						element = elementDAO.SelectChampsElement(element);
					}
					else
					{
						elementDAO.DeleteLeElement(element);
						elementDAO.InsertLeElement(element);
						element = elementDAO.SelectChampsElement(element);
					}
				}
				else
				{
					element = null;
				}

				LesElements.Add(element);
			}

			foreach(Element E in LesElements)
			{
				if(E == null)
				{
					LesElements = null;
				}
			}

			facture.Elements = LesElements;

			//validOption 
			facture.Voption = false;

			if (checkBoxOption.Checked)
			{
				facture.Voption = true;
			}

			//TVA 
			facture.TVA = textBoxTVA.Text;

			if(IndiceFacturation != null)
			{
				facture.IndiceFacture = IndiceFacturation;

			}else if(this.LaFacture.IndiceFacture != null)
			{
				facture.IndiceFacture = LaFacture.IndiceFacture;
			} 

			//TypeFacture
			facture.TypeD = this.LaFacture.TypeD;

			if(facture.Numero != null && facture.TypeD != null)
			{
				bool verif = true;
				if(facture.Client != null)
				{
					if (facture.Client.Nom == "" || facture.Client.Adresse == "")
					{
						MessageBox.Show("les informations du client sont invalides, erreur d\'insertion");
						verif = false;
					}


					if (facture.Elements != null)
					{
						if (facture.Elements.Count == 0)
						{
							MessageBox.Show("la liste d'élément du devis ne peut être vide, erreur d\'insertion");
							verif = false;
						}
					}
					else
					{
						MessageBox.Show("un ou plusieurs éléments sont invalides, erreur d\'insertion");
						verif = false;
					}

				if (facture.DateTime == null)
					{
						MessageBox.Show("la date est invalide, erreur d\'insertion");
						verif = false;
					}
					if (facture.TVA == "" || facture.TVA == null)
					{
						MessageBox.Show("la TVA est invalide, erreur d\'insertion");
						verif = false;
					}
				}
				else
				{
					MessageBox.Show("les informations du client sont obligatoires, erreur d\'insertion");
					verif = false;
				}

				if (facture.Elements != null)
				{
					for (int i = 0; i < facture.Elements.Count - 1; i++)
					{
						if (facture.Elements[i].Id == facture.Elements[i + 1].Id)
						{
							MessageBox.Show("Deux élements ne peuvent pas être identiques");
							verif = false;
						}
					}
				}
				


				if (verif == true)
				{
					if (FactureDAO.VerifFactureIfExist(facture))
					{
						FactureDAO.DeleteFacture(facture);
						if (facture.NumeroDevis == null)
						{
							FactureDAO.InsertLaFacture(facture);
						}
						else
						{
							FactureDAO.InsertLaFactureduDevis(facture);
						}
						

						facture = FactureDAO.SelectChampsFacture(facture);
						if (FactureDAO.AjoutElement(facture))
						{
							MessageBox.Show("Modification de la Facture réussie");
							DevisFacileListFacture devisFacileListFacture = new DevisFacileListFacture(DevisFacileAccueil);
							devisFacileListFacture.Show();
							Close();
						}

					}
					else
					{
						if (facture.NumeroDevis == null)
						{
							FactureDAO.InsertLaFacture(facture);
						}
						else
						{
							FactureDAO.InsertLaFactureduDevis(facture);
						}

						facture = FactureDAO.SelectChampsFacture(facture);
						if (FactureDAO.AjoutElement(facture))
						{
							MessageBox.Show("Création de la Facture réussie");
							DevisFacileAccueil.WindowState = FormWindowState.Normal;
							Close();
						}
					}


				}
			}
		}

		
}
}

