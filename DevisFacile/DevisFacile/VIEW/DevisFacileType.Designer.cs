﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileType
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelTitre = new System.Windows.Forms.Label();
			this.Picture = new System.Windows.Forms.PictureBox();
			this.btnRetour = new System.Windows.Forms.Button();
			this.btnProfessionnel = new System.Windows.Forms.Button();
			this.btnParticulier = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
			this.SuspendLayout();
			// 
			// labelTitre
			// 
			this.labelTitre.AutoEllipsis = true;
			this.labelTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTitre.ForeColor = System.Drawing.Color.Blue;
			this.labelTitre.Location = new System.Drawing.Point(2, 38);
			this.labelTitre.Name = "labelTitre";
			this.labelTitre.Size = new System.Drawing.Size(201, 36);
			this.labelTitre.TabIndex = 1;
			this.labelTitre.Text = "Type de devis";
			// 
			// Picture
			// 
			this.Picture.Location = new System.Drawing.Point(195, 12);
			this.Picture.Name = "Picture";
			this.Picture.Size = new System.Drawing.Size(165, 89);
			this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Picture.TabIndex = 7;
			this.Picture.TabStop = false;
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.ForeColor = System.Drawing.Color.Black;
			this.btnRetour.Location = new System.Drawing.Point(8, 12);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 80;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
			// 
			// btnProfessionnel
			// 
			this.btnProfessionnel.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnProfessionnel.Location = new System.Drawing.Point(195, 126);
			this.btnProfessionnel.Name = "btnProfessionnel";
			this.btnProfessionnel.Size = new System.Drawing.Size(165, 116);
			this.btnProfessionnel.TabIndex = 81;
			this.btnProfessionnel.Text = "Professionnel";
			this.btnProfessionnel.UseVisualStyleBackColor = true;
			this.btnProfessionnel.Click += new System.EventHandler(this.btnProfessionnel_Click);
			// 
			// btnParticulier
			// 
			this.btnParticulier.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnParticulier.Location = new System.Drawing.Point(12, 126);
			this.btnParticulier.Name = "btnParticulier";
			this.btnParticulier.Size = new System.Drawing.Size(165, 116);
			this.btnParticulier.TabIndex = 82;
			this.btnParticulier.Text = "Particulier";
			this.btnParticulier.UseVisualStyleBackColor = true;
			this.btnParticulier.Click += new System.EventHandler(this.btnParticulier_Click);
			// 
			// DevisFacileType
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(372, 264);
			this.Controls.Add(this.btnParticulier);
			this.Controls.Add(this.btnProfessionnel);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.Picture);
			this.Controls.Add(this.labelTitre);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.Black;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileType";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DevisFacileType";
			this.Load += new System.EventHandler(this.DevisFacileType_Load);
			((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Label labelTitre;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Button btnRetour;
		private System.Windows.Forms.Button btnProfessionnel;
		private System.Windows.Forms.Button btnParticulier;
	}
}

