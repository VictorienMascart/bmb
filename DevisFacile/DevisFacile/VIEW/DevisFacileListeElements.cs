﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileListeElements : Form
	{
		public Devis LeDevis;

		public DevisFacileCreation DevisFacileCreation;

		public DevisFacileCreationFacture DevisFacileCreationFacture;

		public ElementDAO ElementDAO = new ElementDAO();

		List<Element> LesElements;

		public DevisFacileListeElements(DevisFacileCreation devisFacileCreation)
		{
			DevisFacileCreation = devisFacileCreation;
			InitializeComponent();
		}

		public DevisFacileListeElements(DevisFacileCreationFacture devisFacileCreationFacture)
		{
			DevisFacileCreationFacture = devisFacileCreationFacture;
			InitializeComponent();
		}

		private void DevisFacileListeElements_Load(object sender, EventArgs e)
		{			

			if (ElementDAO.GetElements().Count() == 0)
			{
				if (MessageBox.Show("Aucun Libellé enregistré"
						, "Base de données vide"
						, MessageBoxButtons.OK
						, MessageBoxIcon.Warning) == DialogResult.OK)
				{
					Close();
				}
			}
			else
			{
				LesElements = ElementDAO.GetElements();

				foreach (Element E in LesElements)
				{
					listBoxElements.Items.Add(E);
				}
			}
		}

		private void BtnChoisir_Click(object sender, EventArgs e)
		{
			Element element = (Element)listBoxElements.SelectedItem;

			if(DevisFacileCreation != null)
				DevisFacileCreation.DataGridViewValue(element);
			else
				DevisFacileCreationFacture.DataGridViewValue(element);

			this.Close();
		}

		private void Btnsuppr_Click(object sender, EventArgs e)
		{
			Element LeElement = (Element)listBoxElements.SelectedItem;

			if (LeElement != null)
			{
				if (MessageBox.Show("Vous êtes sur le point de supprimer cette élément"
						, "Confirmation suppression"
						, MessageBoxButtons.YesNo
						, MessageBoxIcon.Warning) == DialogResult.No)
				{
					return;
				}
				else
				{
					bool suppression = ElementDAO.DeleteLeElement(LeElement);

					LesElements = ElementDAO.GetElements();

					listBoxElements.Items.Clear();

					foreach (Element E in LesElements)
					{
						listBoxElements.Items.Add(E);
					}


				}
			}
		}

		private void btnRetour_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}

