﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileListeClients : Form
	{
		public Devis LeDevis;

		public DevisFacileCreation DevisFacileCreation;

		public ClientDAO clientDAO = new ClientDAO();

		List<Client> LesClients;

		public DevisFacileCreationFacture DevisFacileCreationFacture;

		public Facture LaFacture;


		public DevisFacileListeClients(Devis leDevis, DevisFacileCreation devisFacileCreation)
		{
			DevisFacileCreation = devisFacileCreation;
			LeDevis = leDevis;
			InitializeComponent();
		}

		public DevisFacileListeClients(Facture facture, DevisFacileCreationFacture devisFacileCreationFacture)
		{
			DevisFacileCreationFacture = devisFacileCreationFacture;
			LaFacture = facture;
			InitializeComponent();
		}

		private void DevisFacileListeClients_Load(object sender, EventArgs e)
		{ 

			LesClients = clientDAO.GetClients();

			if (LesClients.Count() == 0)
			{
				if(MessageBox.Show("Aucun client enregistré"
					    , "Base de données vide"
						, MessageBoxButtons.OK
						, MessageBoxIcon.Warning) == DialogResult.OK)
				{
					Close();
				}
			}
			else
			{
				foreach (Client c in LesClients)
				{
					listBoxClient.Items.Add(c);
				}
			}
		}

		private void Btnsuppr_Click(object sender, EventArgs e)
		{

			Client LeClient = (Client)listBoxClient.SelectedItem;

			if (LeClient != null)
			{
				if (MessageBox.Show("Vous êtes sur le point de supprimer ce client"
						, "Confirmation suppression"
						, MessageBoxButtons.YesNo
						, MessageBoxIcon.Warning) == DialogResult.No)
				{
					return;
				}
				else
				{
					bool suppression = clientDAO.DeleteLeClient(LeClient);

					LesClients = clientDAO.GetClients();

					listBoxClient.Items.Clear();

					foreach (Client c in LesClients)
					{
						listBoxClient.Items.Add(c);
					}


				}
			}


		}

		private void BtnChoisir_Click(object sender, EventArgs e)
		{
			Client LeClient = (Client)listBoxClient.SelectedItem;
			if (DevisFacileCreation != null)
			{
				if (LeClient != null)
				{
					DevisFacileCreation.TxtNomClientValue = LeClient.Nom;
					DevisFacileCreation.TxtAdresseClientValue = LeClient.Adresse;
				}
			}
			else
			{
				DevisFacileCreationFacture.TxtNomClientValue = LeClient.Nom;
				DevisFacileCreationFacture.TxtAdresseClientValue = LeClient.Adresse;
			}
			Close();
		}

		private void BtnRetour_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
