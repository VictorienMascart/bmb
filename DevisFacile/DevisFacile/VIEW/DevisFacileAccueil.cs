﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.VIEW;
using DevisFacile.MODEL.DAO;
using DevisFacile.MODEL.CLASSE;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileAccueil : Form
	{
		public DevisDAO DevisDAO = new DevisDAO();

		public DevisFacileAccueil()
		{
			InitializeComponent();
		}


		private void DevisFacileAccueil_Load(object sender, EventArgs e)
		{

            //Test MySqlConnection
            //TestDAO testDAO = new TestDAO();
            //if (testDAO.TestConnexion())
            //    MessageBox.Show("ok");

            //Test MySqlCommand

            //testDAO.TestValue();



			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");
		}

		private void btnDevis_Click(object sender, EventArgs e)
		{
			DevisFacileChoixList devisFacileChoixList = new DevisFacileChoixList(this);
			devisFacileChoixList.Show();
			this.WindowState = FormWindowState.Minimized;
		}

		private void btnCreationDevis_Click(object sender, EventArgs e)
		{
			Devis devis = new Devis();
			DevisFacileType devisFacileType = new DevisFacileType(this, devis);
			devisFacileType.Show();
			this.WindowState = FormWindowState.Minimized;
		}

		private void btnCreationFacture_Click(object sender, EventArgs e)
		{
			Facture facture = new Facture();
			DevisFacileType devisFacileType = new DevisFacileType(this, facture);
			devisFacileType.Show();
			this.WindowState = FormWindowState.Minimized;
		}

        private void btnAvoir_Click(object sender, EventArgs e)
        {
            DevisFacileListAvoir devisFacileListAvoir = new DevisFacileListAvoir(this);
            devisFacileListAvoir.Show();
            this.WindowState = FormWindowState.Minimized;
        }

    }
}
