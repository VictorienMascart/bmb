﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileChoixList
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRetour = new System.Windows.Forms.Button();
			this.Picture = new System.Windows.Forms.PictureBox();
			this.btnDevis = new System.Windows.Forms.Button();
			this.btnFacture = new System.Windows.Forms.Button();
			this.labelTitre = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
			this.SuspendLayout();
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.Location = new System.Drawing.Point(10, 12);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 26;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
			// 
			// Picture
			// 
			this.Picture.Location = new System.Drawing.Point(191, 12);
			this.Picture.Name = "Picture";
			this.Picture.Size = new System.Drawing.Size(165, 89);
			this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Picture.TabIndex = 25;
			this.Picture.TabStop = false;
			// 
			// btnDevis
			// 
			this.btnDevis.BackColor = System.Drawing.Color.White;
			this.btnDevis.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDevis.ForeColor = System.Drawing.Color.Black;
			this.btnDevis.Location = new System.Drawing.Point(191, 116);
			this.btnDevis.Name = "btnDevis";
			this.btnDevis.Size = new System.Drawing.Size(165, 70);
			this.btnDevis.TabIndex = 24;
			this.btnDevis.Text = "Devis";
			this.btnDevis.UseVisualStyleBackColor = true;
			this.btnDevis.Click += new System.EventHandler(this.btnDevis_Click);
			// 
			// btnFacture
			// 
			this.btnFacture.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnFacture.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnFacture.ForeColor = System.Drawing.Color.Black;
			this.btnFacture.Location = new System.Drawing.Point(10, 116);
			this.btnFacture.Name = "btnFacture";
			this.btnFacture.Size = new System.Drawing.Size(165, 70);
			this.btnFacture.TabIndex = 23;
			this.btnFacture.Text = "Facture";
			this.btnFacture.UseVisualStyleBackColor = true;
			this.btnFacture.Click += new System.EventHandler(this.btnFacture_Click);
			// 
			// labelTitre
			// 
			this.labelTitre.AutoEllipsis = true;
			this.labelTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTitre.ForeColor = System.Drawing.Color.Blue;
			this.labelTitre.Location = new System.Drawing.Point(4, 48);
			this.labelTitre.Name = "labelTitre";
			this.labelTitre.Size = new System.Drawing.Size(181, 36);
			this.labelTitre.TabIndex = 22;
			this.labelTitre.Text = "Choix Liste ";
			// 
			// DevisFacileChoixList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(368, 195);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.Picture);
			this.Controls.Add(this.btnDevis);
			this.Controls.Add(this.btnFacture);
			this.Controls.Add(this.labelTitre);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileChoixList";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DevisFacileChoixList";
			this.Load += new System.EventHandler(this.DevisFacileChoixList_Load);
			((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnRetour;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Button btnDevis;
		private System.Windows.Forms.Button btnFacture;
		private System.Windows.Forms.Label labelTitre;
	}
}