﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileChoixList : Form
	{
		DevisDAO devisDAO = new DevisDAO();
		FactureDAO factureDAO = new FactureDAO();

		public DevisFacileAccueil DevisFacileAccueil;

		public DevisFacileChoixList()
		{
			InitializeComponent();
		}

		public DevisFacileChoixList(DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}


		private void DevisFacileChoixList_Load(object sender, EventArgs e)
		{
			//Image 
			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");
		}


		private void btnRetour_Click(object sender, EventArgs e)
		{
			DevisFacileAccueil.WindowState = FormWindowState.Normal;
			Close();
		}

		private void btnDevis_Click(object sender, EventArgs e)
		{
            DevisFacileListAvoir devisFacileListDevis = new DevisFacileListAvoir(DevisFacileAccueil);
			if (devisDAO.GetDevis().Count != 0)
			{
				devisFacileListDevis.Show();
				this.Close();
			}
		}

		private void btnFacture_Click(object sender, EventArgs e)
		{
			DevisFacileListFacture DevisFacileListFacture = new DevisFacileListFacture(DevisFacileAccueil);
			if (factureDAO.GetFacture().Count != 0)
			{
				DevisFacileListFacture.Show();
				this.Close();
			}
		}

	}
}

