﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileListFacture : Form
	{
		public DevisFacileAccueil DevisFacileAccueil;

		FactureDAO factureDAO = new FactureDAO();
		List<Facture> ListFacture = new List<Facture>();

		public DevisFacileListFacture(DevisFacileAccueil devisFacileAccueil)
		{
			this.DevisFacileAccueil = new DevisFacileAccueil();
			this.DevisFacileAccueil = devisFacileAccueil;

			InitializeComponent();
		}

		public DevisFacileListFacture()
		{
			InitializeComponent();
		}

		private void DevisFacileListFacture_Load(object sender, EventArgs e)
		{			
			ListFacture = factureDAO.GetFacture();

			ElementDAO elementDAO = new ElementDAO();

			Facture facture;

			foreach (Facture f in ListFacture)
			{
				facture = elementDAO.GetElementsInFacture(f);
				listFacture.Items.Add(facture);
			}

		}

		private void BtnSuppr_Click(object sender, EventArgs e)
		{
			if (listFacture.SelectedItem != null)
			{
				Facture facture = new Facture();
				facture = (Facture)listFacture.SelectedItem;


				if (facture != null)
				{
					if (MessageBox.Show("Vous êtes sur le point de supprimer cette facture"
							, "Confirmation suppression"
							, MessageBoxButtons.YesNo
							, MessageBoxIcon.Warning) == DialogResult.No)
					{
						return;
					}
					else
					{
						bool suppression = factureDAO.DeleteFacture(facture);
						/*
						if (suppression == false)
						{
							throw new Exception("Erreur de suppression");
						}
						*/
						ListFacture = factureDAO.GetFacture();

						listFacture.Items.Clear();

						foreach (Facture d in ListFacture)
						{
							listFacture.Items.Add(d);
						}


					}
				}
			}	
		}

		private void BtnModif_Click(object sender, EventArgs e)
		{
			if (listFacture.SelectedItem != null)
			{
				Facture LaFacture = (Facture)listFacture.SelectedItem;

				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(LaFacture, this, DevisFacileAccueil);
				devisFacileCreationFacture.ShowDialog();


				Close();
			}
			
		}

		private void BtnRetour_Click(object sender, EventArgs e)
		{
			DevisFacileChoixList devisFacileChoixList = new DevisFacileChoixList(DevisFacileAccueil);
			devisFacileChoixList.Show();
			this.Close();
		}

		private void btnOuvrir_Click(object sender, EventArgs e)
		{
			if (listFacture.SelectedItem != null)
			{
				Facture facture = (Facture)listFacture.SelectedItem;
				CreationFacturePDF creationFacturePDF = new CreationFacturePDF(facture);

				Document document = creationFacturePDF.CreateDocument();


				if(!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\Professionnel"))
				{
					Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\Professionnel");
				}
				if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\Particulier"))
				{
					Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\Particulier");
				}

				string filename;

				if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\"+ facture.Client.Nom))
				{
					if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\" + facture.TypeD.Libelle + "\\" + facture.Client.Nom))
					{
						Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\" + facture.TypeD.Libelle + "\\" + facture.Client.Nom + "\\Devis\\");
						Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\" + facture.TypeD.Libelle + "\\" + facture.Client.Nom + "\\Facture\\");
					}

					filename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\" + facture.TypeD.Libelle + "\\" + facture.Client.Nom + "\\Facture\\Facture2015_" + facture.Numero.Substring(5) + ".pdf";
				}
				else
				{
					filename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\BMB\\" + facture.Client.Nom + "\\Facture2015_" + facture.Numero.Substring(5) + ".pdf";
				}
				
				PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfFontEmbedding.Always);
				renderer.Document = document;
				renderer.RenderDocument();
				try
				{
					renderer.PdfDocument.Save(filename);
					Process.Start(filename as string);
				}
				catch
				{
					MessageBox.Show("Le document concerné est en cours d'utilisation");
				}
			}
		}
	}
}
