﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileAccueil
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Picture = new System.Windows.Forms.PictureBox();
            this.btnDevis = new System.Windows.Forms.Button();
            this.btnCreationDevis = new System.Windows.Forms.Button();
            this.btnCreationFacture = new System.Windows.Forms.Button();
            this.btnAvoir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(148, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Devis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(87, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "création de devis automatisé";
            // 
            // Picture
            // 
            this.Picture.Location = new System.Drawing.Point(422, 12);
            this.Picture.Name = "Picture";
            this.Picture.Size = new System.Drawing.Size(211, 91);
            this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Picture.TabIndex = 2;
            this.Picture.TabStop = false;
            // 
            // btnDevis
            // 
            this.btnDevis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDevis.Location = new System.Drawing.Point(12, 141);
            this.btnDevis.Name = "btnDevis";
            this.btnDevis.Size = new System.Drawing.Size(140, 116);
            this.btnDevis.TabIndex = 11;
            this.btnDevis.Text = "voir mes Devis/Factures";
            this.btnDevis.UseVisualStyleBackColor = true;
            this.btnDevis.Click += new System.EventHandler(this.btnDevis_Click);
            // 
            // btnCreationDevis
            // 
            this.btnCreationDevis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreationDevis.Location = new System.Drawing.Point(468, 141);
            this.btnCreationDevis.Name = "btnCreationDevis";
            this.btnCreationDevis.Size = new System.Drawing.Size(140, 116);
            this.btnCreationDevis.TabIndex = 12;
            this.btnCreationDevis.Text = "créer un Devis";
            this.btnCreationDevis.UseVisualStyleBackColor = true;
            this.btnCreationDevis.Click += new System.EventHandler(this.btnCreationDevis_Click);
            // 
            // btnCreationFacture
            // 
            this.btnCreationFacture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreationFacture.Location = new System.Drawing.Point(322, 141);
            this.btnCreationFacture.Name = "btnCreationFacture";
            this.btnCreationFacture.Size = new System.Drawing.Size(140, 116);
            this.btnCreationFacture.TabIndex = 13;
            this.btnCreationFacture.Text = "créer une Facture";
            this.btnCreationFacture.UseVisualStyleBackColor = true;
            this.btnCreationFacture.Click += new System.EventHandler(this.btnCreationFacture_Click);
            // 
            // btnAvoir
            // 
            this.btnAvoir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAvoir.Location = new System.Drawing.Point(158, 141);
            this.btnAvoir.Name = "btnAvoir";
            this.btnAvoir.Size = new System.Drawing.Size(140, 116);
            this.btnAvoir.TabIndex = 14;
            this.btnAvoir.Text = "liste des avoirs";
            this.btnAvoir.UseVisualStyleBackColor = true;
            this.btnAvoir.Click += new System.EventHandler(this.btnAvoir_Click);
            // 
            // DevisFacileAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(645, 269);
            this.Controls.Add(this.btnAvoir);
            this.Controls.Add(this.btnCreationFacture);
            this.Controls.Add(this.btnCreationDevis);
            this.Controls.Add(this.btnDevis);
            this.Controls.Add(this.Picture);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DevisFacileAccueil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DevisFacile";
            this.Load += new System.EventHandler(this.DevisFacileAccueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Button btnDevis;
		private System.Windows.Forms.Button btnCreationDevis;
		private System.Windows.Forms.Button btnCreationFacture;
        private System.Windows.Forms.Button btnAvoir;
    }
}