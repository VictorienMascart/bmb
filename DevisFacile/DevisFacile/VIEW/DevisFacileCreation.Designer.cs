﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileCreation
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbNumero = new System.Windows.Forms.Label();
            this.btnLibelle = new System.Windows.Forms.Button();
            this.btnPDF = new System.Windows.Forms.Button();
            this.Picture = new System.Windows.Forms.PictureBox();
            this.lbClient = new System.Windows.Forms.Label();
            this.txtNomClient = new System.Windows.Forms.TextBox();
            this.txtAdresseClient = new System.Windows.Forms.TextBox();
            this.lbIdentifiant = new System.Windows.Forms.Label();
            this.lbAdresse = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textHorsTaxes = new System.Windows.Forms.TextBox();
            this.textBoxTVA = new System.Windows.Forms.TextBox();
            this.textBoxTTC = new System.Windows.Forms.TextBox();
            this.checkBoxOption = new System.Windows.Forms.CheckBox();
            this.lbMode = new System.Windows.Forms.Label();
            this.lbModeReglement = new System.Windows.Forms.Label();
            this.lbcommande = new System.Windows.Forms.Label();
            this.lblivraison = new System.Windows.Forms.Label();
            this.txtCommande = new System.Windows.Forms.TextBox();
            this.txtLivraison = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Libellé = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unité = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Quantité = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pxuniteuros = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Totaleuros = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Option = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Supprimer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSelectClient = new System.Windows.Forms.Button();
            this.richTextBoxType = new System.Windows.Forms.RichTextBox();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lbdescription = new System.Windows.Forms.Label();
            this.txtDescriptionTravaux = new System.Windows.Forms.TextBox();
            this.btnRetour = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // lbNumero
            // 
            this.lbNumero.AutoSize = true;
            this.lbNumero.Location = new System.Drawing.Point(19, 164);
            this.lbNumero.Name = "lbNumero";
            this.lbNumero.Size = new System.Drawing.Size(70, 13);
            this.lbNumero.TabIndex = 3;
            this.lbNumero.Text = "LabelNumero";
            // 
            // btnLibelle
            // 
            this.btnLibelle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLibelle.Location = new System.Drawing.Point(7, 185);
            this.btnLibelle.Name = "btnLibelle";
            this.btnLibelle.Size = new System.Drawing.Size(140, 23);
            this.btnLibelle.TabIndex = 13;
            this.btnLibelle.Text = "ajouter un libellé existant";
            this.btnLibelle.UseVisualStyleBackColor = true;
            this.btnLibelle.Click += new System.EventHandler(this.BtnAjoutElement_Click);
            // 
            // btnPDF
            // 
            this.btnPDF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPDF.Location = new System.Drawing.Point(999, 699);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(118, 52);
            this.btnPDF.TabIndex = 15;
            this.btnPDF.Text = "éditer le Devis";
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.BtnPDF_Click);
            // 
            // Picture
            // 
            this.Picture.Location = new System.Drawing.Point(7, 41);
            this.Picture.Name = "Picture";
            this.Picture.Size = new System.Drawing.Size(165, 89);
            this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Picture.TabIndex = 17;
            this.Picture.TabStop = false;
            // 
            // lbClient
            // 
            this.lbClient.AutoSize = true;
            this.lbClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClient.Location = new System.Drawing.Point(268, 29);
            this.lbClient.Name = "lbClient";
            this.lbClient.Size = new System.Drawing.Size(47, 13);
            this.lbClient.TabIndex = 18;
            this.lbClient.Text = "Client :";
            // 
            // txtNomClient
            // 
            this.txtNomClient.Location = new System.Drawing.Point(346, 55);
            this.txtNomClient.Name = "txtNomClient";
            this.txtNomClient.Size = new System.Drawing.Size(236, 20);
            this.txtNomClient.TabIndex = 19;
            // 
            // txtAdresseClient
            // 
            this.txtAdresseClient.Location = new System.Drawing.Point(346, 81);
            this.txtAdresseClient.Multiline = true;
            this.txtAdresseClient.Name = "txtAdresseClient";
            this.txtAdresseClient.Size = new System.Drawing.Size(236, 60);
            this.txtAdresseClient.TabIndex = 20;
            // 
            // lbIdentifiant
            // 
            this.lbIdentifiant.AutoSize = true;
            this.lbIdentifiant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIdentifiant.Location = new System.Drawing.Point(293, 58);
            this.lbIdentifiant.Name = "lbIdentifiant";
            this.lbIdentifiant.Size = new System.Drawing.Size(35, 13);
            this.lbIdentifiant.TabIndex = 21;
            this.lbIdentifiant.Text = "Nom :";
            // 
            // lbAdresse
            // 
            this.lbAdresse.AutoSize = true;
            this.lbAdresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAdresse.Location = new System.Drawing.Point(293, 84);
            this.lbAdresse.Name = "lbAdresse";
            this.lbAdresse.Size = new System.Drawing.Size(51, 13);
            this.lbAdresse.TabIndex = 22;
            this.lbAdresse.Text = "Adresse :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 717);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Base Hors Taxes €";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 717);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "TVA  €";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(277, 717);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "TTC  €";
            // 
            // textHorsTaxes
            // 
            this.textHorsTaxes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textHorsTaxes.Location = new System.Drawing.Point(7, 733);
            this.textHorsTaxes.Name = "textHorsTaxes";
            this.textHorsTaxes.Size = new System.Drawing.Size(100, 13);
            this.textHorsTaxes.TabIndex = 31;
            this.textHorsTaxes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textHorsTaxes.TextChanged += new System.EventHandler(this.TextHorsTaxes_TextChanged);
            // 
            // textBoxTVA
            // 
            this.textBoxTVA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTVA.Location = new System.Drawing.Point(126, 733);
            this.textBoxTVA.Name = "textBoxTVA";
            this.textBoxTVA.Size = new System.Drawing.Size(100, 13);
            this.textBoxTVA.TabIndex = 32;
            this.textBoxTVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxTTC
            // 
            this.textBoxTTC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTTC.Location = new System.Drawing.Point(248, 733);
            this.textBoxTTC.Name = "textBoxTTC";
            this.textBoxTTC.Size = new System.Drawing.Size(100, 13);
            this.textBoxTTC.TabIndex = 33;
            this.textBoxTTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxOption
            // 
            this.checkBoxOption.AutoSize = true;
            this.checkBoxOption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBoxOption.Location = new System.Drawing.Point(7, 688);
            this.checkBoxOption.Name = "checkBoxOption";
            this.checkBoxOption.Size = new System.Drawing.Size(111, 17);
            this.checkBoxOption.TabIndex = 34;
            this.checkBoxOption.Text = "Valider les options";
            this.checkBoxOption.UseVisualStyleBackColor = true;
            this.checkBoxOption.CheckedChanged += new System.EventHandler(this.CheckBoxOption_CheckedChanged);
            // 
            // lbMode
            // 
            this.lbMode.AutoSize = true;
            this.lbMode.Location = new System.Drawing.Point(623, 17);
            this.lbMode.Name = "lbMode";
            this.lbMode.Size = new System.Drawing.Size(56, 13);
            this.lbMode.TabIndex = 35;
            this.lbMode.Text = "labelMode";
            // 
            // lbModeReglement
            // 
            this.lbModeReglement.AutoSize = true;
            this.lbModeReglement.Location = new System.Drawing.Point(488, 695);
            this.lbModeReglement.Name = "lbModeReglement";
            this.lbModeReglement.Size = new System.Drawing.Size(56, 13);
            this.lbModeReglement.TabIndex = 36;
            this.lbModeReglement.Text = "labelMode";
            // 
            // lbcommande
            // 
            this.lbcommande.AutoSize = true;
            this.lbcommande.Location = new System.Drawing.Point(508, 717);
            this.lbcommande.Name = "lbcommande";
            this.lbcommande.Size = new System.Drawing.Size(130, 13);
            this.lbcommande.TabIndex = 37;
            this.lbcommande.Text = "40% à la commande, soit :";
            // 
            // lblivraison
            // 
            this.lblivraison.AutoSize = true;
            this.lblivraison.Location = new System.Drawing.Point(508, 739);
            this.lblivraison.Name = "lblivraison";
            this.lblivraison.Size = new System.Drawing.Size(105, 13);
            this.lblivraison.TabIndex = 38;
            this.lblivraison.Text = "60% à livraison, soit :";
            // 
            // txtCommande
            // 
            this.txtCommande.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCommande.Location = new System.Drawing.Point(644, 717);
            this.txtCommande.Name = "txtCommande";
            this.txtCommande.Size = new System.Drawing.Size(100, 13);
            this.txtCommande.TabIndex = 39;
            // 
            // txtLivraison
            // 
            this.txtLivraison.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLivraison.Location = new System.Drawing.Point(644, 739);
            this.txtLivraison.Name = "txtLivraison";
            this.txtLivraison.Size = new System.Drawing.Size(100, 13);
            this.txtLivraison.TabIndex = 40;
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.Color.Silver;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Libellé,
            this.Unité,
            this.Quantité,
            this.Pxuniteuros,
            this.Totaleuros,
            this.Option,
            this.Supprimer,
            this.id});
            this.dataGridView.Location = new System.Drawing.Point(7, 214);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(993, 468);
            this.dataGridView.TabIndex = 41;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellContentClick);
            this.dataGridView.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellLeave);
            this.dataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellValueChanged);
            this.dataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DataGridView_RowsAdded);
            // 
            // Libellé
            // 
            this.Libellé.FillWeight = 160F;
            this.Libellé.HeaderText = "Libellé";
            this.Libellé.Name = "Libellé";
            this.Libellé.Width = 350;
            // 
            // Unité
            // 
            this.Unité.HeaderText = "Unité";
            this.Unité.Items.AddRange(new object[] {
            "U",
            "m2",
            "m3",
            "mm",
            "cm",
            "m",
            "L",
            "ml",
            "Kg",
            "T"});
            this.Unité.Name = "Unité";
            this.Unité.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Unité.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Quantité
            // 
            this.Quantité.HeaderText = "Quantité";
            this.Quantité.Name = "Quantité";
            // 
            // Pxuniteuros
            // 
            this.Pxuniteuros.HeaderText = "Px unit. euros";
            this.Pxuniteuros.Name = "Pxuniteuros";
            // 
            // Totaleuros
            // 
            this.Totaleuros.HeaderText = "Total euros";
            this.Totaleuros.Name = "Totaleuros";
            this.Totaleuros.ReadOnly = true;
            // 
            // Option
            // 
            this.Option.HeaderText = "Option";
            this.Option.Name = "Option";
            // 
            // Supprimer
            // 
            this.Supprimer.HeaderText = "";
            this.Supprimer.Name = "Supprimer";
            this.Supprimer.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Supprimer.Text = "Supprimer";
            this.Supprimer.UseColumnTextForButtonValue = true;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // btnSelectClient
            // 
            this.btnSelectClient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectClient.Location = new System.Drawing.Point(346, 26);
            this.btnSelectClient.Name = "btnSelectClient";
            this.btnSelectClient.Size = new System.Drawing.Size(125, 23);
            this.btnSelectClient.TabIndex = 42;
            this.btnSelectClient.Text = "Selectionner un client existant";
            this.btnSelectClient.UseVisualStyleBackColor = true;
            this.btnSelectClient.Click += new System.EventHandler(this.BtnSelectClient_Click);
            // 
            // richTextBoxType
            // 
            this.richTextBoxType.BackColor = System.Drawing.Color.White;
            this.richTextBoxType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxType.Location = new System.Drawing.Point(626, 33);
            this.richTextBoxType.Name = "richTextBoxType";
            this.richTextBoxType.Size = new System.Drawing.Size(495, 148);
            this.richTextBoxType.TabIndex = 43;
            this.richTextBoxType.Text = "";
            this.richTextBoxType.TextChanged += new System.EventHandler(this.richTextBoxType_TextChanged);
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate.Location = new System.Drawing.Point(243, 161);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(101, 13);
            this.txtDate.TabIndex = 44;
            this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbdescription
            // 
            this.lbdescription.AutoSize = true;
            this.lbdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdescription.Location = new System.Drawing.Point(220, 190);
            this.lbdescription.Name = "lbdescription";
            this.lbdescription.Size = new System.Drawing.Size(155, 13);
            this.lbdescription.TabIndex = 45;
            this.lbdescription.Text = "Descriptions des travaux :";
            // 
            // txtDescriptionTravaux
            // 
            this.txtDescriptionTravaux.Location = new System.Drawing.Point(377, 187);
            this.txtDescriptionTravaux.Name = "txtDescriptionTravaux";
            this.txtDescriptionTravaux.Size = new System.Drawing.Size(593, 20);
            this.txtDescriptionTravaux.TabIndex = 46;
            // 
            // btnRetour
            // 
            this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetour.Location = new System.Drawing.Point(7, 12);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(75, 23);
            this.btnRetour.TabIndex = 48;
            this.btnRetour.Text = "retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.BtnRetour_Click);
            // 
            // DevisFacileCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1129, 759);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.txtDescriptionTravaux);
            this.Controls.Add(this.lbdescription);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.richTextBoxType);
            this.Controls.Add(this.btnSelectClient);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.txtLivraison);
            this.Controls.Add(this.txtCommande);
            this.Controls.Add(this.lblivraison);
            this.Controls.Add(this.lbcommande);
            this.Controls.Add(this.lbModeReglement);
            this.Controls.Add(this.lbMode);
            this.Controls.Add(this.checkBoxOption);
            this.Controls.Add(this.textBoxTTC);
            this.Controls.Add(this.textBoxTVA);
            this.Controls.Add(this.textHorsTaxes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbAdresse);
            this.Controls.Add(this.lbIdentifiant);
            this.Controls.Add(this.txtAdresseClient);
            this.Controls.Add(this.txtNomClient);
            this.Controls.Add(this.lbClient);
            this.Controls.Add(this.Picture);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.btnLibelle);
            this.Controls.Add(this.lbNumero);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DevisFacileCreation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DevisFacileCreation";
            this.Load += new System.EventHandler(this.DevisFacileCreation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label lbNumero;
		private System.Windows.Forms.Button btnLibelle;
		private System.Windows.Forms.Button btnPDF;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Label lbClient;
		private System.Windows.Forms.TextBox txtNomClient;
		private System.Windows.Forms.TextBox txtAdresseClient;
		private System.Windows.Forms.Label lbIdentifiant;
		private System.Windows.Forms.Label lbAdresse;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textHorsTaxes;
		private System.Windows.Forms.TextBox textBoxTVA;
		private System.Windows.Forms.TextBox textBoxTTC;
		private System.Windows.Forms.CheckBox checkBoxOption;
		private System.Windows.Forms.Label lbMode;
		private System.Windows.Forms.Label lbModeReglement;
		private System.Windows.Forms.Label lbcommande;
		private System.Windows.Forms.Label lblivraison;
		private System.Windows.Forms.TextBox txtCommande;
		private System.Windows.Forms.TextBox txtLivraison;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.Button btnSelectClient;
		private System.Windows.Forms.RichTextBox richTextBoxType;
		private System.Windows.Forms.TextBox txtDate;
		private System.Windows.Forms.Label lbdescription;
		private System.Windows.Forms.TextBox txtDescriptionTravaux;
		private System.Windows.Forms.Button btnRetour;
		private System.Windows.Forms.DataGridViewTextBoxColumn Libellé;
		private System.Windows.Forms.DataGridViewComboBoxColumn Unité;
		private System.Windows.Forms.DataGridViewTextBoxColumn Quantité;
		private System.Windows.Forms.DataGridViewTextBoxColumn Pxuniteuros;
		private System.Windows.Forms.DataGridViewTextBoxColumn Totaleuros;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Option;
		private System.Windows.Forms.DataGridViewButtonColumn Supprimer;
		private System.Windows.Forms.DataGridViewTextBoxColumn id;
	}
}