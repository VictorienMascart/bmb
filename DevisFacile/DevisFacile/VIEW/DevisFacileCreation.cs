﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;


namespace DevisFacile.VIEW
{
	public partial class DevisFacileCreation : Form
	{
		public DevisFacileAccueil DevisFacileAccueil;
		public Devis Ledevis;
		public ListAvoir DevisFacileListDevis;
		ElementDAO elementDAO = new ElementDAO();
		//DevisTypeDAO TypeDevisDao = new DevisTypeDAO();


		//propriété du txtNomClient
		public string TxtNomClientValue
		{
			get
			{
				return this.txtNomClient.Text;
			}

			set
			{
				this.txtNomClient.Text = value;
			}
		}
		public string TxtAdresseClientValue
		{
			get
			{
				return this.txtAdresseClient.Text;
			}

			set
			{
				this.txtAdresseClient.Text = value;
			}
		}

		public void DataGridViewValue(Element element)
		{
			if (element != null)
			{
				decimal total = element.Prix_unit * element.Quantite;

				string[] row = { element.Libelle, element.Unite.ToString(), element.Quantite.ToString(), element.Prix_unit.ToString(), total.ToString(), element.IsOption.ToString(),"supprimer", element.Id.ToString()};

				dataGridView.Rows.Add(row);

			}
			
		}

		public DevisFacileCreation(Devis Ledevis)
		{
			this.Ledevis = Ledevis;
			InitializeComponent();
		}

		public DevisFacileCreation(Devis Ledevis, DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			this.Ledevis = Ledevis;
			InitializeComponent();
		}

		public DevisFacileCreation(Devis Ledevis, ListAvoir devisFacileListDevis, DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			DevisFacileListDevis = devisFacileListDevis;
			this.Ledevis = Ledevis;
			InitializeComponent();
		}

		public DevisFacileCreation()
		{
			InitializeComponent();
		}

		private void DevisFacileCreation_Load(object sender, EventArgs e)
		{
			DevisDAO devisDAO = new DevisDAO();

			
			//Image 
			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");

			if (Ledevis != null && DevisFacileListDevis != null)
			{
				// si le devis existe 

				lbNumero.Text = "Devis : " + Ledevis.Numero;

				ClientDAO clientDAO = new ClientDAO();
				Ledevis.Client = clientDAO.SelectChampsClient(Ledevis.Client);

				txtNomClient.Text = Ledevis.Client.Nom;
				txtAdresseClient.Text = Ledevis.Client.Adresse;
				string date = Ledevis.Date.ToString().Substring(0, 10);
				txtDate.Text = date;
				txtDescriptionTravaux.Text = Ledevis.DescriptionTravaux;

				Ledevis = elementDAO.GetElementsInDevis(Ledevis);

				if(Ledevis.Elements.Count > 0)
				{
					foreach (Element E in Ledevis.Elements)
					{
						DataGridViewValue(E);
					}
				}

				DevisTypeDAO devisTypeDAO = new DevisTypeDAO();

				Ledevis.TypeD = devisTypeDAO.SelectChampsType(Ledevis.TypeD);

				richTextBoxType.Text = Ledevis.TypeD.Description.ToString();

				textBoxTVA.Text = Ledevis.Tva.ToString();

				if (Ledevis.TypeD.Libelle == "Professionnel")
				{
					lbMode.Text = "MARCHÉ - RÉGLEMENTS";
					lbMode.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);

					lbModeReglement.Visible = false;
					lblivraison.Visible = false;
					lbcommande.Visible = false;
					txtCommande.Visible = false;
					txtLivraison.Visible = false;
				}
				else
				{
					lbMode.Text = "RÉGLEMENTS";
					lbMode.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);

					lbModeReglement.Text = "MODE DE REGLEMENTS € :";
					lbModeReglement.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);
				}

				if(Ledevis.ValidOption)
				{
					checkBoxOption.Checked = true;
				}

			}
			else
			{

				//si création devis

				//Numero Devis

				if (devisDAO.GetLastNumeroDevis() != null) {
					string num = devisDAO.GetLastNumeroDevis();

					string numero = num.Substring(num.Length - 3);

					int LastNumero = Convert.ToInt32(numero);

					LastNumero = LastNumero + 1;

					//afficher le numero
					lbNumero.Text = "Devis : " + "2014" + "/" + LastNumero;
				}
				else
				{
					lbNumero.Text = "Devis : " + "2014" + "/" + "400";
				}
				

				//afficher la date
				string now = DateTime.Now.ToString();
				now = now.Substring(0, 10);
				txtDate.Text = now;

				//label en fonction du type de contrat 
				DevisTypeDAO devisTypeDAO = new DevisTypeDAO();

				if (Ledevis.TypeD.Libelle == "Professionnel")
				{
					lbMode.Text = "MARCHÉ - RÉGLEMENTS";
					lbMode.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);

					lbModeReglement.Visible = false;
					lblivraison.Visible = false;
					lbcommande.Visible = false;
					txtCommande.Visible = false;
					txtLivraison.Visible = false;

					if (devisTypeDAO.SelectDescription(Ledevis.TypeD.Libelle) != "")
					{
						richTextBoxType.Text = devisTypeDAO.SelectDescription(Ledevis.TypeD.Libelle);
					}
					else
					{
						richTextBoxType.Text = "-Suivant Norme Française NF P03-001 \n" +
								   "-A défaut de conditions de paiements acceptées par les 2 parties contractantes, l'acceptation de ce devis \n" +
								   " vaut acceptation par le client de nos conditions générales de règlements fixées à 30 jours.\n" +
								   "-Tout retard de paiement non justifié sera soumis à des pénalités correspondant à l'application du taux \n" +
								   " EONIA + 10 points. \n" +
								   "-L'acceptation de ce devis vaut acceptation par le client des conditions prévues au CCAG \n" +
								   " Travaux.Toutes clauses dérogatoires seront réputées non écrites.";
					}
						
				}
				else if (Ledevis.TypeD.Libelle == "Particulier")
				{
					lbMode.Text = "RÉGLEMENTS";
					lbMode.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);


					if (devisTypeDAO.SelectDescription(Ledevis.TypeD.Libelle) != "")
					{
						richTextBoxType.Text = devisTypeDAO.SelectDescription(Ledevis.TypeD.Libelle);
					}
					else
					{
						richTextBoxType.Text = "-A défaut de conditions de paiements acceptées par les 2 parties contractantes, l'acceptation de ce devis \n" +
								   " vaut acceptation par le client de nos conditions générales de règlements fixées à 30 jours.\n" +
								   "-Tout retard de paiement non justifié sera soumis à des pénalités correspondant à l'application du taux \n" +
								   " EONIA + 10 points. \n" +
								   "-L'acceptation de ce devis vaut acceptation par le client des conditions prévues au CCAG \n" +
								   " Travaux.Toutes clauses dérogatoires seront réputées non écrites.";
					}
					

					lbModeReglement.Text = "MODE DE REGLEMENTS € :";
					lbModeReglement.Font = new Font(lbMode.Font.Name, 8, FontStyle.Bold | FontStyle.Underline);
				}

				textBoxTVA.Text = Ledevis.Tva;

			}
		}

		private void BtnAjoutElement_Click(object sender, EventArgs e)
		{
			DevisFacileListeElements devisFacileListeElements = new DevisFacileListeElements(this);
			devisFacileListeElements.ShowDialog();
		}

		private void BtnSelectClient_Click(object sender, EventArgs e)
		{
			DevisFacileListeClients devisFacileListeClients = new DevisFacileListeClients(Ledevis, this);
			devisFacileListeClients.ShowDialog();
		}

		private void BtnPDF_Click(object sender, EventArgs e)
		{
			//créer les elements du devis
			List<Element> elements = new List<Element>();

			for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
			{
				Unite unite = new Unite();
				if (dataGridView.Rows[i].Cells[1].Value != null)
				{
					unite.Libelle = dataGridView.Rows[i].Cells[1].Value.ToString();
				}
				if (unite.Libelle !=null && unite.Libelle != "")
				{
					UniteDAO uniteDAO = new UniteDAO();
					unite = uniteDAO.SelectChampsUnite(unite);
				}				

				string libelle = (string)dataGridView.Rows[i].Cells[0].Value;
				bool quantiteVerif = decimal.TryParse((string)dataGridView.Rows[i].Cells[2].Value, out decimal quantite);
				bool prixUnitVerif = decimal.TryParse((string)dataGridView.Rows[i].Cells[3].Value, out decimal prixU);

				bool check = false;

				if (dataGridView.Rows[i].Cells[5].Value.GetType().Equals(typeof(string)))
				{
					string val = dataGridView.Rows[i].Cells[5].Value as string;

					if (val == "True")
					{
						check = true;
					}

				}
				else if (dataGridView.Rows[i].Cells[5].Value.GetType().Equals(typeof(bool)))
				{
					bool val = (bool)dataGridView.Rows[i].Cells[5].Value;

					if (val == true)
					{
						check = true;
					}
				}
				int id = Convert.ToInt32(dataGridView.Rows[i].Cells[7].Value);

				if (quantiteVerif == true && prixUnitVerif == true && unite != null)
				{
					
					if (unite != null && libelle !=null &&  unite.Libelle != null && libelle != "")
					{

						Element element = new Element(id, libelle, unite, quantite, prixU, check);

						elements.Add(element);

						if (this.elementDAO.IfExistLeElement(element) == true)
						{
							this.elementDAO.DeleteLeElement(element);
							this.elementDAO.InsertLeElement(element);
							element = this.elementDAO.SelectChampsElement(element);
						}
						else
						{
							this.elementDAO.InsertLeElement(element);
							element = this.elementDAO.SelectChampsElement(element);
						}
					}
					else
					{
						MessageBox.Show("un ou plusieurs éléments sont invalides, erreur d\'insertion");
						elements = null;
					}

				}
				else
				{
					MessageBox.Show("un ou plusieurs éléments sont invalides, erreur d\'insertion");
					elements = null;
				}
				
			}

			Ledevis.TypeD.Description = richTextBoxType.Text;

			ClientDAO clientDAO = new ClientDAO();
			Client client = new Client(txtNomClient.Text, txtAdresseClient.Text);

			if (txtNomClient.Text != ""  || txtAdresseClient.Text != "")
			{
			
				if (clientDAO.IfExistLeClient(client) == false)
				{
					clientDAO.InsertLeClient(client);
					client = clientDAO.SelectIdClient(client);
				}
				else
				{
					client = clientDAO.SelectIdClient(client);
				}
			}
			

			bool option = false;

			if (checkBoxOption.Checked)
			{
				option = true;
			}

			string numero = lbNumero.Text.Substring(8);

			Devis devis = new Devis();
			if (elements != null)
			{
				devis = new Devis(numero, Convert.ToDateTime(txtDate.Text), elements, Ledevis.TypeD, client, txtDescriptionTravaux.Text, option, textBoxTVA.Text);

			}

			DevisDAO devisDAO = new DevisDAO();
			ElementDAO elementDAO = new ElementDAO();
			DevisTypeDAO devisTypeDAO = new DevisTypeDAO();

			bool verif = true;
			
			if (devis.Numero != null && devis.TypeD != null && devis.TypeD.Libelle != ""&& lbcommande.Text != "" && txtLivraison.Text != "" && txtCommande.Text != "" && textHorsTaxes.Text != "" && textBoxTTC.Text != "" && richTextBoxType.Text != "")
			{
				if (devis.Client != null)
				{
					if (devis.Client.Nom == "" || devis.Client.Adresse == "")
					{
						MessageBox.Show("les informations du client sont invalides, erreur d\'insertion");
						verif = false;
					}


					if (devis.Elements != null)
					{
						if (devis.Elements.Count == 0)
						{
							MessageBox.Show("la liste d'élément du devis ne peut être vide, erreur d\'insertion");
							verif = false;
						}
					}
					else
					{
						MessageBox.Show("un ou plusieurs éléments sont invalides, erreur d\'insertion");
						verif = false;
					}

					if (devis.Date == null)
					{
						MessageBox.Show("la date est invalide, erreur d\'insertion");
						verif = false;
					}
					if (devis.Tva == "" || devis.Tva == null)
					{
						MessageBox.Show("la TVA est invalide, erreur d\'insertion");
						verif = false;
					}
				}
				else
				{
					MessageBox.Show("les informations du client sont obligatoires, erreur d\'insertion");
					verif = false;
				}

				if (devis.Elements != null)
				{
					for (int i = 0; i < devis.Elements.Count - 1; i++)
					{
						if (devis.Elements[i].Id == devis.Elements[i + 1].Id)
						{
							MessageBox.Show("Deux élements ne peuvent pas être identiques");
							verif = false;
						}
					}
				}


				if (devisDAO.VerifDevisIfExist(devis) == false && verif == true)
				{
					if (devisDAO.InsertLeDevis(devis))
					{
						devis = devisDAO.SelectChampsDevis(devis);
						devisDAO.AjoutElement(devis);
						devisTypeDAO.UpdateDescription(devis.TypeD);
						MessageBox.Show("création du Devis réussie");
						DevisFacileAccueil.WindowState = FormWindowState.Normal;
						Close();
					}
				}
				else if(devisDAO.VerifDevisIfExist(devis) == true && verif == true)
				{
					devisDAO.DeleteDevis(devis);
					if (devisDAO.InsertLeDevis(devis))
					{
						devis = devisDAO.SelectChampsDevis(devis);
						devisDAO.AjoutElement(devis);
						devisTypeDAO.UpdateDescription(devis.TypeD);
						MessageBox.Show("Modification du Devis réussie");
						DevisFacileListAvoir devisFacileListDevis = new DevisFacileListAvoir(DevisFacileAccueil);
						devisFacileListDevis.Show();
						Close();
					}
				}
			}
		}

		public bool IsNumeric(string Nombre)
		{
			try
			{
				double.Parse(Nombre);
				return true;
			}
			catch
			{
				return false;
			}
		}

		private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridView.RowCount > 0)
			{
				if (dataGridView.CurrentCell != null && (e.ColumnIndex == 2 || e.ColumnIndex == 3))
				{
					int L = dataGridView.CurrentCell.RowIndex;

					if (dataGridView.Rows[L].Cells[2].Value != null && dataGridView.Rows[L].Cells[3].Value != null)
					{
						if (IsNumeric((string)dataGridView.Rows[L].Cells[2].Value) && IsNumeric((string)dataGridView.Rows[L].Cells[3].Value))
						{
							double value = Math.Round(Convert.ToDouble(dataGridView.Rows[L].Cells[2].Value) * Convert.ToDouble(dataGridView.Rows[L].Cells[3].Value), 2);
							dataGridView.Rows[L].Cells[4].Value = value;
						}
					}

					if (!IsNumeric((string)dataGridView.Rows[L].Cells[2].Value))
						dataGridView.Rows[L].Cells[2].Value = null;
					if (!IsNumeric((string)dataGridView.Rows[L].Cells[3].Value))
						dataGridView.Rows[L].Cells[3].Value = null;
				}

				//calcule du hors taxes et ttc
				if (e.ColumnIndex == 5 || e.ColumnIndex == 4)
				{
					double somme = 0;
					if (!checkBoxOption.Checked)
					{
						

						for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
						{
							bool check = false;
							string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

							if (strValue == "True")
							{
								check = true;
							}

							if (check == false)
								somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

						}

						textHorsTaxes.Text = somme.ToString();
					}
					else
					{
						for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
						{
							somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

							dataGridView.Rows[i].Cells[5].ReadOnly = true;
						}

						textHorsTaxes.Text = somme.ToString();
					}
					

					if (textBoxTVA.Text == "auto liquidation")
					{
						textBoxTTC.Text = textHorsTaxes.Text;
					}
					else
					{
						double ttc = 0;
						if (textBoxTVA.Text == "10%")
						{
							ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

						}
						else
						{
							ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
						}
						textBoxTTC.Text = ttc.ToString();

						txtCommande.Text = ((((double)40 / 100) * ttc)).ToString();
						txtLivraison.Text = ((((double)60 / 100) * ttc)).ToString();
					}
				}
			}
		}


		private void DataGridView_CellLeave(object sender, DataGridViewCellEventArgs e)
		{
			if(dataGridView.RowCount > 0)
			{
				if ((e.ColumnIndex == 2 || e.ColumnIndex == 3))
				{
					if (!IsNumeric((string)dataGridView.CurrentCell.Value))
					{
						dataGridView.CurrentCell.Value = null;
					}
				}
			}
		}

		private void DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.ColumnIndex == 6 && dataGridView.RowCount > 1 && !dataGridView.Rows[dataGridView.CurrentCell.RowIndex].IsNewRow)
			{
				dataGridView.Rows.Remove(dataGridView.CurrentRow);
			}
		}

		private void DataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{ 
			for (int i = 0; i < dataGridView.Rows.Count -1; i++)
			{
				dataGridView.Rows[i].Cells[6].Value = "supprimer";
			}



				//calcule du hors taxes et ttc
				double somme = 0;
			if (!checkBoxOption.Checked)
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					if (dataGridView.Rows[i].Cells[5].Value != null)
					{
						bool check = false;
						string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

						if (strValue == "True")
						{
							check = true;
						}

						if (check == false)
						{
							somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);
						}
					}
					else
					{
						dataGridView.Rows[i].Cells[5].Value = false;
					}
				}
			}
			else
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					if (dataGridView.Rows[i].Cells[5].Value != null)
					{
						somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

						dataGridView.Rows[i].Cells[5].ReadOnly = true;
					}
					else
					{
						dataGridView.Rows[i].Cells[5].Value = false;
					}
				}
			}
			
			textHorsTaxes.Text = somme.ToString();
			if (textBoxTVA.Text == "auto liquidation")
			{
				textBoxTTC.Text = textHorsTaxes.Text;
			}
			else
			{
				double ttc = 0;
				if (textBoxTVA.Text == "10%")
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

				}
				else
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
				}
				textBoxTTC.Text = ttc.ToString();

				txtCommande.Text = ((((double)40 / 100) * ttc)).ToString();
				txtLivraison.Text = ((((double)60 / 100) * ttc)).ToString();
			}

		}

		private void BtnRetour_Click(object sender, EventArgs e)
		{
			DevisDAO devisDAO = new DevisDAO();
			if (Ledevis != null && DevisFacileListDevis != null)
			{
                DevisFacileListAvoir DevisFacileListDevis = new DevisFacileListAvoir(DevisFacileAccueil);
				DevisFacileListDevis.Show();
				this.Close();
			}
			else
			{
				DevisFacileChoixTVA devisFacileChoixTVA = new DevisFacileChoixTVA(Ledevis, DevisFacileAccueil);
				devisFacileChoixTVA.Show();
				this.Close();
			}
		}

		private void CheckBoxOption_CheckedChanged(object sender, EventArgs e)
		{
			double somme = 0;

			if (checkBoxOption.Checked)
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
						somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);

						dataGridView.Rows[i].Cells[5].ReadOnly = true;
				}
				for (int i = 0; i < dataGridView.Rows.Count; i++)
				{
					dataGridView.Rows[i].Cells[5].ReadOnly = true;
				}
					
			}
			else
			{
				for (int i = 0; i < dataGridView.Rows.Count - 1; i++)
				{
					
					bool check = false;
					string strValue = dataGridView.Rows[i].Cells[5].Value.ToString();

					if (strValue == "True")
					{
						check = true;
					}
					if (check == false)
					{
						somme += Convert.ToDouble(dataGridView.Rows[i].Cells[4].Value);
					}
				}

				for (int i = 0; i < dataGridView.Rows.Count; i++)
				{
					dataGridView.Rows[i].Cells[5].ReadOnly = false;
				}
					
			}

			textHorsTaxes.Text = somme.ToString();
		}

		private void TextHorsTaxes_TextChanged(object sender, EventArgs e)
		{
			//calcule du hors taxes et ttc
			if (textBoxTVA.Text == "auto liquidation")
			{
				textBoxTTC.Text = textHorsTaxes.Text;
			}
			else
			{
				double ttc = 0;
				if (textBoxTVA.Text == "10%")
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)10 / 100) * Convert.ToDouble(textHorsTaxes.Text));

				}
				else
				{
					ttc = Convert.ToDouble(textHorsTaxes.Text) + (((double)20 / 100) * Convert.ToDouble(textHorsTaxes.Text));
				}
				textBoxTTC.Text = ttc.ToString();

				txtCommande.Text = ((((double)40 / 100) * ttc)).ToString();
				txtLivraison.Text = ((((double)60 / 100) * ttc)).ToString();
			}
		}

        private void richTextBoxType_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
