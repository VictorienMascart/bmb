﻿namespace DevisFacile.VIEW
{
	partial class ListAvoir
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.listAvoirs = new System.Windows.Forms.ListBox();
            this.btnOuvrir = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.btnGenerer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listAvoirs
            // 
            this.listAvoirs.BackColor = System.Drawing.Color.White;
            this.listAvoirs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listAvoirs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listAvoirs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listAvoirs.ForeColor = System.Drawing.Color.Black;
            this.listAvoirs.FormattingEnabled = true;
            this.listAvoirs.Location = new System.Drawing.Point(12, 12);
            this.listAvoirs.Name = "listAvoirs";
            this.listAvoirs.Size = new System.Drawing.Size(298, 429);
            this.listAvoirs.TabIndex = 0;
            // 
            // btnOuvrir
            // 
            this.btnOuvrir.BackColor = System.Drawing.Color.White;
            this.btnOuvrir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOuvrir.ForeColor = System.Drawing.Color.Black;
            this.btnOuvrir.Location = new System.Drawing.Point(318, 12);
            this.btnOuvrir.Name = "btnOuvrir";
            this.btnOuvrir.Size = new System.Drawing.Size(149, 23);
            this.btnOuvrir.TabIndex = 1;
            this.btnOuvrir.Text = "Générer l\'avoir en PDF";
            this.btnOuvrir.UseVisualStyleBackColor = true;
            
            // 
            // btnSuppr
            // 
            this.btnSuppr.BackColor = System.Drawing.Color.White;
            this.btnSuppr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSuppr.ForeColor = System.Drawing.Color.Black;
            this.btnSuppr.Location = new System.Drawing.Point(318, 83);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(149, 23);
            this.btnSuppr.TabIndex = 4;
            this.btnSuppr.Text = "Supprimer l\'avoir";
            this.btnSuppr.UseVisualStyleBackColor = true;
            
            // 
            // btnRetour
            // 
            this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetour.Location = new System.Drawing.Point(392, 415);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(75, 23);
            this.btnRetour.TabIndex = 79;
            this.btnRetour.Text = "retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // btnGenerer
            // 
            this.btnGenerer.BackColor = System.Drawing.Color.White;
            this.btnGenerer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerer.ForeColor = System.Drawing.Color.Black;
            this.btnGenerer.Location = new System.Drawing.Point(318, 41);
            this.btnGenerer.Name = "btnGenerer";
            this.btnGenerer.Size = new System.Drawing.Size(149, 36);
            this.btnGenerer.TabIndex = 80;
            this.btnGenerer.Text = "Générer les avoirs de cette année";
            this.btnGenerer.UseVisualStyleBackColor = true;
            // 
            // ListAvoir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(479, 450);
            this.Controls.Add(this.btnGenerer);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.btnOuvrir);
            this.Controls.Add(this.listAvoirs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ListAvoir";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListAvoir";
            
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox listAvoirs;
		private System.Windows.Forms.Button btnOuvrir;
		private System.Windows.Forms.Button btnSuppr;
		private System.Windows.Forms.Button btnRetour;
        private System.Windows.Forms.Button btnGenerer;
    }
}