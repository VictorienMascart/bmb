﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileTypeFacture
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnFactureComplette = new System.Windows.Forms.Button();
			this.btnFactureAcompte = new System.Windows.Forms.Button();
			this.btnFactureLivraison = new System.Windows.Forms.Button();
			this.Picture = new System.Windows.Forms.PictureBox();
			this.labelTitre = new System.Windows.Forms.Label();
			this.btnRetour = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
			this.SuspendLayout();
			// 
			// btnFactureComplette
			// 
			this.btnFactureComplette.Location = new System.Drawing.Point(9, 156);
			this.btnFactureComplette.Name = "btnFactureComplette";
			this.btnFactureComplette.Size = new System.Drawing.Size(141, 83);
			this.btnFactureComplette.TabIndex = 0;
			this.btnFactureComplette.Text = "Facture Complette";
			this.btnFactureComplette.UseVisualStyleBackColor = true;
			this.btnFactureComplette.Click += new System.EventHandler(this.btnFactureComplette_Click);
			// 
			// btnFactureAcompte
			// 
			this.btnFactureAcompte.Location = new System.Drawing.Point(156, 156);
			this.btnFactureAcompte.Name = "btnFactureAcompte";
			this.btnFactureAcompte.Size = new System.Drawing.Size(141, 83);
			this.btnFactureAcompte.TabIndex = 1;
			this.btnFactureAcompte.Text = "Facture d\'acompte";
			this.btnFactureAcompte.UseVisualStyleBackColor = true;
			this.btnFactureAcompte.Click += new System.EventHandler(this.btnFactureAcompte_Click);
			// 
			// btnFactureLivraison
			// 
			this.btnFactureLivraison.Location = new System.Drawing.Point(303, 156);
			this.btnFactureLivraison.Name = "btnFactureLivraison";
			this.btnFactureLivraison.Size = new System.Drawing.Size(141, 83);
			this.btnFactureLivraison.TabIndex = 2;
			this.btnFactureLivraison.Text = "Bon de livraison";
			this.btnFactureLivraison.UseVisualStyleBackColor = true;
			this.btnFactureLivraison.Click += new System.EventHandler(this.btnFactureLivraison_Click);
			// 
			// Picture
			// 
			this.Picture.Location = new System.Drawing.Point(279, 26);
			this.Picture.Name = "Picture";
			this.Picture.Size = new System.Drawing.Size(165, 89);
			this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Picture.TabIndex = 8;
			this.Picture.TabStop = false;
			// 
			// labelTitre
			// 
			this.labelTitre.AutoEllipsis = true;
			this.labelTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTitre.ForeColor = System.Drawing.Color.Blue;
			this.labelTitre.Location = new System.Drawing.Point(11, 52);
			this.labelTitre.Name = "labelTitre";
			this.labelTitre.Size = new System.Drawing.Size(262, 36);
			this.labelTitre.TabIndex = 9;
			this.labelTitre.Text = "Mode de Facturation";
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.ForeColor = System.Drawing.Color.Black;
			this.btnRetour.Location = new System.Drawing.Point(9, 12);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 81;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
			// 
			// DevisFacileTypeFacture
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(456, 253);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.labelTitre);
			this.Controls.Add(this.Picture);
			this.Controls.Add(this.btnFactureLivraison);
			this.Controls.Add(this.btnFactureAcompte);
			this.Controls.Add(this.btnFactureComplette);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileTypeFacture";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DevisFacileTypeFacture";
			this.Load += new System.EventHandler(this.DevisFacileTypeFacture_Load);
			((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnFactureComplette;
		private System.Windows.Forms.Button btnFactureAcompte;
		private System.Windows.Forms.Button btnFactureLivraison;
		private System.Windows.Forms.PictureBox Picture;
		private System.Windows.Forms.Label labelTitre;
		private System.Windows.Forms.Button btnRetour;
	}
}