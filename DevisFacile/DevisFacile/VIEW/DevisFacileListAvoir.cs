﻿using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevisFacile.VIEW
{
    public partial class DevisFacileListAvoir : Form
    {
        public DevisFacileAccueil DevisFacileAccueil { get; set; }

        public BindingSource bindingSource { get; set; } = new BindingSource();

        public DevisFacileListAvoir(DevisFacileAccueil devisFacileAccueil)
        {
            DevisFacileAccueil = devisFacileAccueil;
            InitializeComponent();
        }

        private void DevisFacileListAvoir_Load(object sender, EventArgs e)
        {
            listAvoirs.DataSource = bindingSource;
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            DevisFacileAccueil.WindowState = FormWindowState.Normal;
            this.Close();
        }

        private void btnSuppr_Click(object sender, EventArgs e)
        {
            AvoirDAO avoirDAO = new AvoirDAO();

            if (listAvoirs.SelectedItem != null)
            {
                Avoir avoir = new Avoir();
                avoir = (Avoir)listAvoirs.SelectedItem;

                if (avoir != null)
                {
                    if (MessageBox.Show("Vous êtes sur le point de supprimer cet avoir"
                            , "Confirmation suppression"
                            , MessageBoxButtons.YesNo
                            , MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        return;
                    }
                    else
                    {
                        bool suppression = avoirDAO.SupprimerLeAvoir(avoir.Id);
                        /*
						if (suppression == false)
						{
							throw new Exception("Erreur de suppression");
						}
						*/

                        bindingSource.DataSource = avoirDAO.GenererLesAvoirs();

                    }
                }
            }
        }

        private void btnGenereravoir_Click_1(object sender, EventArgs e)
        {

            AvoirDAO avoirDAO = new AvoirDAO();
            bindingSource.DataSource = avoirDAO.GenererLesAvoirs();

        }
    }
}
