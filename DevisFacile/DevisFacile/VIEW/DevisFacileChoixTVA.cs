﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileChoixTVA : Form
	{
		protected Devis LeDevis;
		protected Facture LaFacture;

		public DevisFacileAccueil DevisFacileAccueil;

		public DevisFacileChoixTVA()
		{
			InitializeComponent();
		}

		public DevisFacileChoixTVA(Devis LeDevis)
		{
			this.LeDevis = LeDevis;
			InitializeComponent();
		}

		public DevisFacileChoixTVA(DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		public DevisFacileChoixTVA(Facture laFacture, DevisFacileAccueil devisFacileAccueil)
		{
			LaFacture = laFacture;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		public DevisFacileChoixTVA(Devis leDevis, DevisFacileAccueil devisFacileAccueil)
		{
			LeDevis = leDevis;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		private void DevisFacileChoixTVA_Load(object sender, EventArgs e)
		{
			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");

			if (LaFacture != null)
			{
				if (LaFacture.TypeD.Libelle == "Professionnel")
				{
					button1.Text = "20%";
					button2.Text = "auto liquidation";

				}
				else if (LaFacture.TypeD.Libelle == "Particulier")
				{
					button1.Text = "10%";
					button2.Text = "20%";
				}
			}else if(LeDevis != null)
			{
				if (LeDevis.TypeD.Libelle == "Professionnel")
				{
					button1.Text = "20%";
					button2.Text = "auto liquidation";

				}
				else if (LeDevis.TypeD.Libelle == "Particulier")
				{
					button1.Text = "10%";
					button2.Text = "20%";
				}
			}
			

			
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			if (LeDevis != null)
			{
				LeDevis.Tva = button1.Text;

				DevisFacileCreation devisFacileCreation = new DevisFacileCreation(LeDevis, DevisFacileAccueil);
				devisFacileCreation.Show();
				this.Close();
			}
			else
			{
				if (LaFacture.TypeD.Libelle == "Particulier")
				{
					LaFacture.TVA = button1.Text;

					DevisFacileTypeFacture DevisFacileTypeFacture = new DevisFacileTypeFacture(LaFacture, DevisFacileAccueil);
					DevisFacileTypeFacture.Show();
					this.Close();
				}
				else
				{
					LaFacture.TVA = button1.Text;
					DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(LaFacture, DevisFacileAccueil, LaFacture.IndiceFacture);
					devisFacileCreationFacture.Show();
					Close();
				}
			}
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			if (LeDevis != null)
			{
				LeDevis.Tva = button2.Text;

				DevisFacileCreation devisFacileCreation = new DevisFacileCreation(LeDevis, DevisFacileAccueil);
				devisFacileCreation.Show();
				this.Close();
			}
			else
			{
				if (LaFacture.TypeD.Libelle == "Particulier")
				{

					LaFacture.TVA = button2.Text;
					DevisFacileTypeFacture DevisFacileTypeFacture = new DevisFacileTypeFacture(LaFacture, DevisFacileAccueil);
					DevisFacileTypeFacture.Show();
					this.Close();
				}
				else
				{
					LaFacture.TVA = button2.Text;
					DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(LaFacture, DevisFacileAccueil, LaFacture.IndiceFacture);
					devisFacileCreationFacture.Show();
					Close();
				}
			}
		}

		private void btnRetour_Click(object sender, EventArgs e)
		{
			if (DevisFacileAccueil != null)
			{
				if(LeDevis != null)
				{
					DevisFacileType devisFacileType = new DevisFacileType(DevisFacileAccueil, LeDevis);
					devisFacileType.Show();
					this.Close();
				}
				else
				{
					DevisFacileType devisFacileType = new DevisFacileType(DevisFacileAccueil, LaFacture);
					devisFacileType.Show();
					this.Close();
				}
				
			}
			else
			{
				Close();
				DevisFacileAccueil.WindowState = FormWindowState.Normal;
			}

		}
	}
}
