﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileListFacture
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listFacture = new System.Windows.Forms.ListBox();
			this.btnOuvrir = new System.Windows.Forms.Button();
			this.btnModif = new System.Windows.Forms.Button();
			this.btnSuppr = new System.Windows.Forms.Button();
			this.btnRetour = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// listFacture
			// 
			this.listFacture.BackColor = System.Drawing.Color.White;
			this.listFacture.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listFacture.Cursor = System.Windows.Forms.Cursors.Hand;
			this.listFacture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listFacture.ForeColor = System.Drawing.Color.Black;
			this.listFacture.FormattingEnabled = true;
			this.listFacture.Location = new System.Drawing.Point(12, 12);
			this.listFacture.Name = "listFacture";
			this.listFacture.Size = new System.Drawing.Size(298, 429);
			this.listFacture.TabIndex = 0;
			// 
			// btnOuvrir
			// 
			this.btnOuvrir.BackColor = System.Drawing.Color.White;
			this.btnOuvrir.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnOuvrir.ForeColor = System.Drawing.Color.Black;
			this.btnOuvrir.Location = new System.Drawing.Point(318, 12);
			this.btnOuvrir.Name = "btnOuvrir";
			this.btnOuvrir.Size = new System.Drawing.Size(149, 23);
			this.btnOuvrir.TabIndex = 1;
			this.btnOuvrir.Text = "générer la facture en PDF";
			this.btnOuvrir.UseVisualStyleBackColor = true;
			this.btnOuvrir.Click += new System.EventHandler(this.btnOuvrir_Click);
			// 
			// btnModif
			// 
			this.btnModif.BackColor = System.Drawing.Color.White;
			this.btnModif.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnModif.ForeColor = System.Drawing.Color.Black;
			this.btnModif.Location = new System.Drawing.Point(318, 41);
			this.btnModif.Name = "btnModif";
			this.btnModif.Size = new System.Drawing.Size(149, 23);
			this.btnModif.TabIndex = 2;
			this.btnModif.Text = "Modifier la facture selectionné";
			this.btnModif.UseVisualStyleBackColor = true;
			this.btnModif.Click += new System.EventHandler(this.BtnModif_Click);
			// 
			// btnSuppr
			// 
			this.btnSuppr.BackColor = System.Drawing.Color.White;
			this.btnSuppr.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnSuppr.ForeColor = System.Drawing.Color.Black;
			this.btnSuppr.Location = new System.Drawing.Point(318, 70);
			this.btnSuppr.Name = "btnSuppr";
			this.btnSuppr.Size = new System.Drawing.Size(149, 23);
			this.btnSuppr.TabIndex = 4;
			this.btnSuppr.Text = "Supprimer la facture";
			this.btnSuppr.UseVisualStyleBackColor = true;
			this.btnSuppr.Click += new System.EventHandler(this.BtnSuppr_Click);
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.Location = new System.Drawing.Point(392, 415);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 79;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.BtnRetour_Click);
			// 
			// DevisFacileListFacture
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(479, 450);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.btnSuppr);
			this.Controls.Add(this.btnModif);
			this.Controls.Add(this.btnOuvrir);
			this.Controls.Add(this.listFacture);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileListFacture";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DevisFacileListFacture";
			this.Load += new System.EventHandler(this.DevisFacileListFacture_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox listFacture;
		private System.Windows.Forms.Button btnOuvrir;
		private System.Windows.Forms.Button btnModif;
		private System.Windows.Forms.Button btnSuppr;
		private System.Windows.Forms.Button btnRetour;
	}
}