﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;

namespace DevisFacile.VIEW
{
	public partial class DevisFacileTypeFacture : Form
	{

		public Facture Facture;
		public DevisFacileAccueil DevisFacileAccueil;
		public ListAvoir DevisFacileListDevis;

		public DevisFacileTypeFacture()
		{
			InitializeComponent();
		}

		public DevisFacileTypeFacture(Facture facture, DevisFacileAccueil devisFacileAccueil)
		{
			Facture = facture;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}

		public DevisFacileTypeFacture(Facture facture, ListAvoir devisFacileTypeFacture,DevisFacileAccueil devisFacileAccueil)
		{
			DevisFacileListDevis = devisFacileTypeFacture;
			Facture = facture;
			DevisFacileAccueil = devisFacileAccueil;
			InitializeComponent();
		}


		private void DevisFacileTypeFacture_Load(object sender, EventArgs e)
		{
			string path = Application.StartupPath;

			Picture.Image = Image.FromFile(path + "/image/bmb_logo.png");
		}

		private void btnRetour_Click(object sender, EventArgs e)
		{
			if(DevisFacileListDevis != null)
			{
                DevisFacileListAvoir devisFacileListDevis = new DevisFacileListAvoir(DevisFacileAccueil);
				devisFacileListDevis.Show();
				Close();
			}
			else
			{
				DevisFacileChoixTVA DevisFacileChoixTVA = new DevisFacileChoixTVA(Facture, DevisFacileAccueil);
				DevisFacileChoixTVA.Show();
				this.Close();
			}
			
		}

		private void btnFactureComplette_Click(object sender, EventArgs e)
		{
			if (DevisFacileListDevis != null)
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FC", DevisFacileListDevis);
				devisFacileCreationFacture.Show();
				Close();
			}
			else
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FC");
				devisFacileCreationFacture.Show();
				Close();
			}
			

		}

		private void btnFactureAcompte_Click(object sender, EventArgs e)
		{
			if (DevisFacileListDevis != null)
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FA", DevisFacileListDevis);
				devisFacileCreationFacture.Show();
				Close();
			}
			else
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FA");
				devisFacileCreationFacture.Show();
				Close();
			}
		}

		private void btnFactureLivraison_Click(object sender, EventArgs e)
		{
			if (DevisFacileListDevis != null)
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FL", DevisFacileListDevis);
				devisFacileCreationFacture.Show();
				Close();
			}
			else
			{
				DevisFacileCreationFacture devisFacileCreationFacture = new DevisFacileCreationFacture(Facture, DevisFacileAccueil, "FL");
				devisFacileCreationFacture.Show();
				Close();
			}
		}
	}
}
