﻿namespace DevisFacile.VIEW
{
	partial class DevisFacileListeElements
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBoxElements = new System.Windows.Forms.ListBox();
			this.btnChoisir = new System.Windows.Forms.Button();
			this.btnsuppr = new System.Windows.Forms.Button();
			this.btnRetour = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// listBoxElements
			// 
			this.listBoxElements.BackColor = System.Drawing.Color.White;
			this.listBoxElements.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxElements.Cursor = System.Windows.Forms.Cursors.Hand;
			this.listBoxElements.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listBoxElements.FormattingEnabled = true;
			this.listBoxElements.Location = new System.Drawing.Point(12, 10);
			this.listBoxElements.Name = "listBoxElements";
			this.listBoxElements.Size = new System.Drawing.Size(797, 572);
			this.listBoxElements.TabIndex = 0;
			// 
			// btnChoisir
			// 
			this.btnChoisir.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnChoisir.Location = new System.Drawing.Point(815, 11);
			this.btnChoisir.Name = "btnChoisir";
			this.btnChoisir.Size = new System.Drawing.Size(75, 23);
			this.btnChoisir.TabIndex = 1;
			this.btnChoisir.Text = "Choisir";
			this.btnChoisir.UseVisualStyleBackColor = true;
			this.btnChoisir.Click += new System.EventHandler(this.BtnChoisir_Click);
			// 
			// btnsuppr
			// 
			this.btnsuppr.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnsuppr.Location = new System.Drawing.Point(815, 40);
			this.btnsuppr.Name = "btnsuppr";
			this.btnsuppr.Size = new System.Drawing.Size(75, 23);
			this.btnsuppr.TabIndex = 3;
			this.btnsuppr.Text = "supprimer";
			this.btnsuppr.UseVisualStyleBackColor = true;
			this.btnsuppr.Click += new System.EventHandler(this.Btnsuppr_Click);
			// 
			// btnRetour
			// 
			this.btnRetour.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnRetour.Location = new System.Drawing.Point(815, 561);
			this.btnRetour.Name = "btnRetour";
			this.btnRetour.Size = new System.Drawing.Size(75, 23);
			this.btnRetour.TabIndex = 80;
			this.btnRetour.Text = "retour";
			this.btnRetour.UseVisualStyleBackColor = true;
			this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
			// 
			// DevisFacileListeElements
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(902, 596);
			this.Controls.Add(this.btnRetour);
			this.Controls.Add(this.btnsuppr);
			this.Controls.Add(this.btnChoisir);
			this.Controls.Add(this.listBoxElements);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DevisFacileListeElements";
			this.Text = "DevisFacileListeElements";
			this.Load += new System.EventHandler(this.DevisFacileListeElements_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox listBoxElements;
		private System.Windows.Forms.Button btnChoisir;
		private System.Windows.Forms.Button btnsuppr;
		private System.Windows.Forms.Button btnRetour;
	}
}