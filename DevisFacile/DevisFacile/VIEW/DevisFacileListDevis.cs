﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;
using DevisFacile.MODEL.DAO;
using PdfSharp.Charting;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp;
using System.Diagnostics;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using System.IO;

namespace DevisFacile.VIEW
{
	public partial class ListAvoir : Form
	{
		public DevisFacileAccueil DevisFacileAccueil;

		
		public ListAvoir()
        {
            InitializeComponent();
        }

		private void btnRetour_Click(object sender, EventArgs e)
		{
			DevisFacileChoixList devisFacileChoixList = new DevisFacileChoixList(DevisFacileAccueil);
			devisFacileChoixList.Show();
			this.Close();
		}
	}
}
