﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevisFacile.MODEL.CLASSE;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
	public class DevisDAO
	{

		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";

		public List<Devis> GetDevis()
		{
			List<Devis> LesDevis = new List<Devis>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				 SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Devis" +
								  " ORDER BY Date;";

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					 while (reader.Read())
					{
						Devis devis = new Devis();

						devis.Id = reader.GetInt32(0);
						devis.Numero = reader.GetString(1);
						devis.Date = reader.GetDateTime(2);

						devis.Client= new Client();
						devis.Client.Id = reader.GetInt32(3);

						devis.TypeD = new CLASSE.TypeD();
						devis.TypeD.Id = reader.GetInt32(4);

						devis.Tva = reader.GetString(5);
						devis.DescriptionTravaux = reader.GetString(6);

						int Voption = reader.GetByte(7);

						devis.ValidOption = Convert.ToBoolean(Voption);

						LesDevis.Add(devis);
					}
				}
				else
				{
					MessageBox.Show("il n'y a aucun Devis dans la base", "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

				connection.Close();

			}catch(Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LesDevis;
		}

		public string GetLastNumeroDevis()
		{
			string num = "";
			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT MAX(Numero) FROM Devis;";

				SqlDataReader reader = cmd.ExecuteReader();
				reader.Read();
				if (reader.IsDBNull(0))
				{
					num = "2014/400";
				}
				else
				{
					num = reader.GetString(0);
				}	

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return num;
		}

		public bool VerifDevisIfExist(Devis devis)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Devis" +
					             " WHERE Numero=@num;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", devis.Numero);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					verif = true;
				}
				else
				{
					verif = false;
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return verif;
		}

		public bool DeleteDevis(Devis devis)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "DELETE FROM [dbo].[Devis]" +
								 " WHERE Numero=@Num;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@Num", devis.Numero);

				int result = cmd.ExecuteNonQuery();

				verif = result != 0;
				
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

			return verif;
		}

		public bool InsertLeDevis(Devis Devis)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "INSERT INTO Devis(Numero,Date,idClient,idType,TVA, DescriptionTravaux, ValidedOption)" +
								 " Values(@num,@date,@idClient,@idType,@TVA,@Desc,@Valid)";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", Devis.Numero);
				cmd.Parameters.AddWithValue("@date", Devis.Date);
				cmd.Parameters.AddWithValue("@idClient", Devis.Client.Id);
				cmd.Parameters.AddWithValue("@idType", Devis.TypeD.Id);
				cmd.Parameters.AddWithValue("@TVA", Devis.Tva);
				cmd.Parameters.AddWithValue("@Desc", Devis.DescriptionTravaux);

				int Voption = 0;
				if (Devis.ValidOption == true)
					Voption = 1;

				cmd.Parameters.AddWithValue("@Valid", Voption);



				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public bool AjoutElement(Devis devis)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);

				foreach (Element e in devis.Elements)
				{
					connection.Open();
					SqlCommand cmd = connection.CreateCommand();

					cmd.CommandText = "INSERT INTO Correspondre(idDevis,idElement)" +
									 " Values (@idDevis,@idElement)";

					int result = 0;
					cmd.Prepare();
					cmd.Parameters.AddWithValue("@idDevis", devis.Id);
					cmd.Parameters.AddWithValue("@idElement", e.Id);

					result = cmd.ExecuteNonQuery();
					verif = result != 0;

					connection.Close();
				}

				

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

			return verif;
		}

		public Devis SelectChampsDevis(Devis devis)
		{
			Devis LeDevis = new Devis();

			LeDevis = devis;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Devis" +
								  " WHERE Numero = @num;";
				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", devis.Numero);
				
				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						LeDevis.Id = reader.GetInt32(0);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LeDevis;
		}
	}
}
