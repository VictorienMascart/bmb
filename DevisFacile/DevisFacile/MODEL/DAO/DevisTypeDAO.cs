﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevisFacile.MODEL.CLASSE;

namespace DevisFacile.MODEL.DAO
{
	public class DevisTypeDAO
	{
		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";
		public string SelectDescription(string libelle)
		{
			string description = "";

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT Description FROM TypeDevis" +
								 " WHERE Libelle=@lib;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@lib", libelle);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						description = reader.GetString(0);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return description;
		}

		public bool UpdateDescription(TypeD typeDevis)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "UPDATE TypeDevis set Description=@desc WHERE Libelle=@lib;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@desc", typeDevis.Description);
				cmd.Parameters.AddWithValue("@lib", typeDevis.Libelle);

				int nbRows = cmd.ExecuteNonQuery();

				if (nbRows != 1)
				{
					throw new Exception("erreur de modification");
				}
				else
				{
					verif = true;
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return verif;
		}

		public TypeD SelectChampsType(TypeD type)
		{
			TypeD typeDevis = new TypeD();

			typeDevis = type;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM TypeDevis" +
								 " WHERE Id=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", type.Id);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						typeDevis.Libelle = reader.GetString(1);
						typeDevis.Description = reader.GetString(2);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return typeDevis;
		}
	}
}
