﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using DevisFacile.MODEL.CLASSE;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DevisFacile.MODEL.DAO
{

    //CLASSE UTILE SI ON UTILISE MySQL

    class TestDAO : DeclarationsDAO
    {

        public bool TestConnexion()
        {
            connection.Open();
            if (connection.Ping())
            {

                connection.Close();
                return true;
            }
            else
            {
                connection.Close();
                return false;
            }

           
        }


        //method(s)
        public bool TestValue()
        {
            try {
                
                connection.Open();

                MySqlCommand cmd = connection.CreateCommand();

                cmd.CommandText = "SELECT * FROM utilisateur" +
                                         " WHERE nom=@nom;";

                cmd.Prepare();

                cmd.Parameters.AddWithValue("@nom", "Bunisset");

                MySqlDataReader reader = cmd.ExecuteReader();

                string chaine = "";

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        chaine = reader.GetInt32(0).ToString() + " " + reader.GetString(1) + " " + reader.GetString(2) + " " + reader.GetString(3);
                        MessageBox.Show(chaine);
                    }   
                }

                

                connection.Close();
                return true;
            }catch (MySqlException ex)
            {
				MessageBox.Show(ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
			}
			
        }
       
    }
}
