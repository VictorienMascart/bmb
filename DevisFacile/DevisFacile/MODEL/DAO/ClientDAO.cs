﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using DevisFacile.MODEL.CLASSE;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
	public class ClientDAO
	{
		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";


		public List<Client> GetClients()
		{

			List<Client> LesClients = new List<Client>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Client order by Nom;";

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						string nom = reader.GetString(1);
						string adresse = reader.GetString(2);

						Client client = new Client(id, nom, adresse);

						LesClients.Add(client);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LesClients;
		}

		


		public bool DeleteLeClient(Client client)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "DELETE FROM [dbo].[Client]" +
								 " WHERE Id=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", client.Id);

				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public bool InsertLeClient(Client client)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "INSERT INTO Client(nom,adresse)" +
								 " Values (@nom,@addr)";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@nom", client.Nom);
				cmd.Parameters.AddWithValue("@addr", client.Adresse);

				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public  Client SelectIdClient(Client Client)
		{
			Client client = new Client();
			try
			{


				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Client" +
								 " WHERE nom=@nom;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@nom", Client.Nom);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						client = new Client(id, Client.Nom,Client.Adresse);
					}
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return client;
		}

		public bool IfExistLeClient(Client Client)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "select * FROM Client" +
								 " WHERE Nom=@nom AND Adresse=@addr;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@nom", Client.Nom);
				cmd.Parameters.AddWithValue("@addr", Client.Adresse);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					verif = true;
				}


				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}


		public Client SelectChampsClient(Client Client)
		{
			Client client = new Client();
			client = Client;
			try
			{


				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Client" +
								 " WHERE Id=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", Client.Id);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						string nom = reader.GetString(1);
						client.Nom = nom;
						string adresse = reader.GetString(2);
						client.Adresse = adresse;
					}
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return client;
		}
	}
}
