﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevisFacile.MODEL.CLASSE;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
	public class FactureDAO
	{

		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";

		public List<Facture> GetFacture()
		{
			List<Facture> LesFacture = new List<Facture>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				 SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Facture" +
								  " ORDER BY Date;";

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					 while (reader.Read())
					 {
						Facture Facture = new Facture();

						Facture.Id = reader.GetInt32(0);
						Facture.Numero = reader.GetString(1);
						Facture.DateTime = reader.GetDateTime(2);

						Facture.Client= new Client();
						Facture.Client.Id = reader.GetInt32(3);

						Facture.TypeD = new CLASSE.TypeD();
						Facture.TypeD.Id = reader.GetInt32(4);

						Facture.TVA = reader.GetString(5);
						Facture.DescriptifTravaux = reader.GetString(6);

						int Voption = reader.GetByte(7);

						Facture.Voption = Convert.ToBoolean(Voption);
						
						Facture.IndiceFacture = reader.GetString(8);

						if (!reader.IsDBNull(9))
						{
							Facture.NumeroDevis = reader.GetString(9);
						}

						LesFacture.Add(Facture);
					 }
				}
				else
				{
					MessageBox.Show("il n'y a aucune Facture dans la base", "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

				connection.Close();

			}catch(Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LesFacture;
		}

		public string GetLastNumeroFacture()
		{
			string num = "";
			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT MAX(Numero) FROM Facture; ";

				SqlDataReader reader = cmd.ExecuteReader();
				reader.Read();
				if (reader.IsDBNull(0))
				{
					num = "2015/250";
				}
				else
				{
					num = reader.GetString(0);
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

			return num;
		}

		public bool VerifFactureIfExist(Facture Facture)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Facture" +
					             " WHERE Numero=@num;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", Facture.Numero);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					verif = true;
				}
				else
				{
					verif = false;
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return verif;
		}

		public bool DeleteFacture(Facture Facture)
		{
			bool verif;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "DELETE FROM [dbo].[Facture]" +
								 " WHERE Numero=@Num;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@Num", Facture.Numero);

				int result = cmd.ExecuteNonQuery();

				verif = result != 0;
				
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning); throw new Exception(ex.ToString());
				
			}

			return verif;
		}

		public bool InsertLaFacture(Facture Facture)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "INSERT INTO Facture(Numero,Date,idClient,idType,TVA, DescriptionTravaux, ValidedOption, IndiceFacture)" +
								 " Values(@num,@date,@idClient,@idType,@TVA,@Desc,@Valid, @fac)";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", Facture.Numero);
				cmd.Parameters.AddWithValue("@date", Facture.DateTime);
				cmd.Parameters.AddWithValue("@idClient", Facture.Client.Id);
				cmd.Parameters.AddWithValue("@idType", Facture.TypeD.Id);
				cmd.Parameters.AddWithValue("@TVA", Facture.TVA);
				cmd.Parameters.AddWithValue("@Desc", Facture.DescriptifTravaux);
				cmd.Parameters.AddWithValue("@fac", Facture.IndiceFacture);

				int Voption = 0;
				if (Facture.Voption == true)
					Voption = 1;

				cmd.Parameters.AddWithValue("@Valid", Voption);



				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public bool InsertLaFactureduDevis(Facture Facture)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "INSERT INTO Facture(Numero,Date,idClient,idType,TVA, DescriptionTravaux, ValidedOption, IndiceFacture, NumeroDevis)" +
								 " Values(@num,@date,@idClient,@idType,@TVA,@Desc,@Valid, @fac, @numDevis)";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", Facture.Numero);
				cmd.Parameters.AddWithValue("@date", Facture.DateTime);
				cmd.Parameters.AddWithValue("@idClient", Facture.Client.Id);
				cmd.Parameters.AddWithValue("@idType", Facture.TypeD.Id);
				cmd.Parameters.AddWithValue("@TVA", Facture.TVA);
				cmd.Parameters.AddWithValue("@Desc", Facture.DescriptifTravaux);
				cmd.Parameters.AddWithValue("@fac", Facture.IndiceFacture);
				cmd.Parameters.AddWithValue("@numDevis", Facture.NumeroDevis);

				int Voption = 0;
				if (Facture.Voption == true)
					Voption = 1;

				cmd.Parameters.AddWithValue("@Valid", Voption);



				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}


		public bool AjoutElement(Facture Facture)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);

				foreach (Element e in Facture.Elements)
				{
					connection.Open();
					SqlCommand cmd = connection.CreateCommand();

					cmd.CommandText = "INSERT INTO Facturer(idFacture,idElement)" +
									 " Values (@idFacture,@idElement)";

					int result = 0;
					cmd.Prepare();
					cmd.Parameters.AddWithValue("@idFacture", Facture.Id);
					cmd.Parameters.AddWithValue("@idElement", e.Id);

					result = cmd.ExecuteNonQuery();
					verif = result != 0;

					connection.Close();
				}

				

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public Facture SelectChampsFacture(Facture Facture)
		{
			Facture LeFacture = new Facture();

			LeFacture = Facture;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Facture" +
								  " WHERE Numero = @num;";
				cmd.Prepare();

				cmd.Parameters.AddWithValue("@num", Facture.Numero);
				
				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						LeFacture.Id = reader.GetInt32(0);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LeFacture;
		}
	}
}
