﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DevisFacile.MODEL.CLASSE;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
	public class ElementDAO
	{
		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";
		public List<Element> GetElements()
		{
			List<Element> LesElements = new List<Element>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Element ORDER BY Id;";

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						string libelle = reader.GetString(1);
						decimal quantite = (decimal)reader.GetValue(2);
						decimal prixU = (decimal)reader.GetValue(3);
						int IdUnite = reader.GetInt32(4);
						int isOption = reader.GetByte(5);

						bool IsOption = Convert.ToBoolean(isOption);

						Unite unite = new Unite(IdUnite);

						Element Element = new Element(id, libelle, unite, quantite, prixU, IsOption);

						LesElements.Add(Element);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LesElements;
		}




		public bool DeleteLeElement(Element Element)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "DELETE FROM [dbo].[Element]" +
								 " WHERE Id=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", Element.Id);

				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public bool IfExistLeElement(Element Element)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "select * FROM Element" +
								 " WHERE libelle=@lib AND quantite=@quantite AND prix_unit = @prix AND idUnit = @idU AND IsOption = @isOp;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@lib", Element.Libelle);
				cmd.Parameters.AddWithValue("@quantite", Element.Quantite);
				cmd.Parameters.AddWithValue("@prix", Element.Prix_unit);
				cmd.Parameters.AddWithValue("@idU", Element.Unite.Id);

				int val;

				if(Element.IsOption == true)
				{
					val = 1;
				}
				else
				{
					val = 0;
				}

				cmd.Parameters.AddWithValue("@isOp", val);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					verif = true;
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public bool InsertLeElement(Element Element)
		{
			bool verif = false;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "INSERT INTO Element(libelle,quantite, prix_unit, idUnit, IsOption)" +
								 " Values (@lib,@quan,@prixU,@idUnit,@isOption)";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@lib", Element.Libelle);
				cmd.Parameters.AddWithValue("@quan", Element.Quantite);
				cmd.Parameters.AddWithValue("@prixU", Element.Prix_unit);
				cmd.Parameters.AddWithValue("@idUnit", Element.Unite.Id);

				int val;

				if (Element.IsOption == true)
				{
					val = 1;
				}
				else
				{
					val = 0;
				}

				cmd.Parameters.AddWithValue("@isOption", val);
				


				int result = cmd.ExecuteNonQuery();

				verif = result != 0;

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return verif;
		}

		public Element SelectChampsElement(Element Element)
		{
			Element LeElement = new Element();

			LeElement = Element;

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "select * FROM Element" +
								 " WHERE libelle=@lib AND quantite=@quantite AND prix_unit = @prix AND idUnit = @idU AND IsOption = @isOp;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@lib", Element.Libelle);
				cmd.Parameters.AddWithValue("@quantite", Element.Quantite);
				cmd.Parameters.AddWithValue("@prix", Element.Prix_unit);
				cmd.Parameters.AddWithValue("@idU", Element.Unite.Id);

				int val;

				if (Element.IsOption == true)
				{
					val = 1;
				}
				else
				{
					val = 0;
				}

				cmd.Parameters.AddWithValue("@isOp", val);

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);

						LeElement.Id = id;


					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}

			return LeElement;
		}

		public Devis GetElementsInDevis(Devis devis)
		{
			Devis LeDevis = new Devis();
			LeDevis = devis;

			LeDevis.Elements = new List<Element>();
			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Element INNER JOIN Correspondre ON idElement = Id WHERE idDevis=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", devis.Id);

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						string libelle = reader.GetString(1);
						decimal quantite = (decimal)reader.GetValue(2);
						decimal prixU = (decimal)reader.GetValue(3);
						int IdUnite = reader.GetInt32(4);
						int isOption = reader.GetByte(5);

						bool IsOption = Convert.ToBoolean(isOption);

						Unite unite = new Unite(IdUnite);

						UniteDAO uniteDAO = new UniteDAO();
						unite = uniteDAO.SelectLaUnite(unite);

						Element Element = new Element(id, libelle, unite, quantite, prixU, IsOption);

						
						LeDevis.Elements.Add(Element);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LeDevis;
		}


		public Facture GetElementsInFacture(Facture facture)
		{
			Facture LaFacture = new Facture();
			
			LaFacture = facture;

			LaFacture.Elements = new List<Element>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Element INNER JOIN Facturer ON idElement = Id WHERE idFacture = @id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", facture.Id);

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						string libelle = reader.GetString(1);
						decimal quantite = (decimal)reader.GetValue(2);
						decimal prixU = (decimal)reader.GetValue(3);
						int IdUnite = reader.GetInt32(4);
						int isOption = reader.GetByte(5);

						bool IsOption = Convert.ToBoolean(isOption);

						Unite unite = new Unite(IdUnite);

						UniteDAO uniteDAO = new UniteDAO();
						unite = uniteDAO.SelectLaUnite(unite);

						Element Element = new Element(id, libelle, unite, quantite, prixU, IsOption);
						
						LaFacture.Elements.Add(Element);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LaFacture;
		}
	}
}
