﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using DevisFacile.MODEL.CLASSE;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
	class UniteDAO
	{
		public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";


		public List<Unite> GetUnites()
		{

			List<Unite> LesUnites = new List<Unite>();

			try
			{
				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Unite;";

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						string libelle = reader.GetString(1);

						Unite unite = new Unite(id, libelle);

						LesUnites.Add(unite);
					}
				}

				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return LesUnites;
		}





		public Unite SelectLaUnite(Unite Unite)
		{
			Unite unite = new Unite();
			try
			{
				

				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Unite" +
								 " WHERE Id=@id;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@id", Unite.Id);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						string libelle = reader.GetString(1);
						unite = new Unite(Unite.Id, libelle);
					}
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return unite;
		}

		public Unite SelectChampsUnite(Unite Unite)
		{
			Unite unite = new Unite();
			try
			{


				SqlConnection connection = new SqlConnection(ConnectionString);
				connection.Open();

				SqlCommand cmd = connection.CreateCommand();

				cmd.CommandText = "SELECT * FROM Unite" +
								 " WHERE libelle = @lib;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@lib", Unite.Libelle);

				SqlDataReader reader = cmd.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						int id = reader.GetInt32(0);
						unite = new Unite(id, Unite.Libelle);
					}
				}
				else
				{
					MessageBox.Show("l'unité "+Unite.Libelle.ToString()+" est invalide", "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					unite = null;
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return unite;
		}
	}
}
