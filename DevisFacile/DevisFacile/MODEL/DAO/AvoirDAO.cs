﻿using DevisFacile.MODEL.CLASSE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevisFacile.MODEL.DAO
{
    class AvoirDAO
    {
        public static string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=" + appDataPath + "\\DevisBMB\\BaseDevis.mdf" + "; Integrated Security=True;";

       
        public List<Avoir> GenererLesAvoirs()
        {
            List<Avoir> avoirs = new List<Avoir>();

            try
            {
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();

                SqlCommand cmd = connection.CreateCommand();

                cmd.CommandText = "SELECT c.nom, f.IdType, SUM(f.MontantTotal) " +
                                  "FROM Facture f " +
                                  "INNER JOIN Client c ON f.idClient = c.Id " +
                                  "WHERE YEAR(f.Date) ="+(DateTime.Now.Year -1 )+" "+
                                  "group by c.nom AND f.idType;";

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string nom = reader.GetString(0);
                        float montant = reader.GetFloat(1);

                        if (reader.GetInt32(3) == 2)  //pro

                            montant = montant - ((montant * 10) / 100);
                        else

                            montant = montant - ((montant * 15) / 100);

                        Avoir avoir = new Avoir();
                        avoir.leClient = new Client();
                        avoir.leClient.Nom = nom;
                        avoir.Montant = montant;

                        avoirs.Add(avoir);
                    }
                }

                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return avoirs;
        }

        public bool SupprimerLeAvoir(int id)
        {
            bool verif = false;
            try
            {
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();

                SqlCommand cmd = connection.CreateCommand();

                cmd.CommandText = "DELETE FROM [dbo].[Avoir]" +
                                 " WHERE Id=@id;";

                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                int result = cmd.ExecuteNonQuery();

                verif = result != 0;

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConnectionString + " " + ex.ToString(), "avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return verif;
        }

    }
}
