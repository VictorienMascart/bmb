﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevisFacile.MODEL.CLASSE
{
	public class TypeD
	{
		//properties
		public int Id { get; set; }
		public string Libelle { get; set; }
		public string Description { get; set; }

		//constructor
		public TypeD(int id, string libelle)
		{
			Id = id;
			Libelle = libelle;
		}

		public TypeD(string libelle, string description)
		{
			Libelle = libelle;
			Description = description;
		}

		public TypeD(){}

		//ToSring
		public override string ToString()
		{
			return Description;
		}
	}
}
