﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.MODEL.CLASSE
{
	public class Element
	{
		public int Id { get; set; }
		public string Libelle { get; set; }
		public Unite Unite { get; set; }
		public decimal Quantite { get; set; }
		public decimal Prix_unit { get; set; }
		public bool IsOption { get; set; }


		public Element(int id, string libelle, Unite unite, decimal quantite, decimal prix_unit, bool isOption)
		{
			Id = id;
			Libelle = libelle;

			UniteDAO uniteDAO = new UniteDAO();
			if(unite.Libelle != null)
			{
				if (uniteDAO.SelectChampsUnite(unite) != null)
				{
					unite = uniteDAO.SelectLaUnite(unite);
					Unite = unite;
				}
			}else
			{
				unite = uniteDAO.SelectLaUnite(unite);
				Unite = unite;
			}

			Quantite = quantite;
			Prix_unit = prix_unit;

			IsOption = isOption;
		}

		public Element(string libelle, Unite unite, decimal quantite, decimal prix_unit, bool isOption)
		{
			Libelle = libelle;

			UniteDAO uniteDAO = new UniteDAO();
			if (uniteDAO.SelectChampsUnite(unite) != null)
			{
				unite = uniteDAO.SelectLaUnite(unite);
				Unite = unite;
			}

			Quantite = quantite;
			Prix_unit = prix_unit;

			IsOption = isOption;
		}

		public Element(){ }

		public override string ToString()
		{
			return Libelle + " /Unité : "+ Unite + " /quantité : "+ Quantite + " /Prix Unit. : " + Prix_unit;
		}

	}
}