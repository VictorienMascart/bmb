﻿using PdfSharp.Charting;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using TabAlignment = MigraDoc.DocumentObjectModel.TabAlignment;
using MigraDoc.DocumentObjectModel.Shapes;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.MODEL.CLASSE
{
	class CreationFacturePDF
	{
		public Facture Facture { get; set; }
		public Document document { get; set; }
		public TextFrame addressFrame { get; set; }

		public CreationFacturePDF(Facture facture)
		{
			if (facture.TypeD.Libelle == null)
			{
				DevisTypeDAO devisTypeDAO = new DevisTypeDAO();
				facture.TypeD = devisTypeDAO.SelectChampsType(facture.TypeD);
			}
			Facture = facture;
		}


		public Document CreateDocument()
		{
			this.document = new Document();
			if (Facture.Numero != null)
			{
				this.document.Info.Title = "Facture 2015-" + Facture.Numero.Substring(5) + ".pdf";
			}
			// Create a new MigraDoc document
			this.document.Info.Subject = "création d'une facture format pdf";
			this.document.Info.Author = "BMB";

			DefineStyles();

			CreatePage();

			return this.document;
		}

		private Table CreatTable(Section section)
		{
			Table table = section.AddTable();
			table.Style = "Table";
			table.Borders.Color = Colors.Black;
			table.Borders.Left.Width = 0.5;
			table.Borders.Right.Width = 0.5;
			table.Borders.Width = 0.25;

			//libelle
			Column column = table.AddColumn(Unit.FromCentimeter(0.5));

			column = table.AddColumn(Unit.FromCentimeter(1));
			column = table.AddColumn(Unit.FromCentimeter(3.5));
			column = table.AddColumn(Unit.FromCentimeter(3));
			column = table.AddColumn(Unit.FromCentimeter(3));


			//unite
			column = table.AddColumn(Unit.FromCentimeter(1));
			column.Format.Alignment = ParagraphAlignment.Center;

			//quantité
			column = table.AddColumn(Unit.FromCentimeter(1));
			column.Format.Alignment = ParagraphAlignment.Center;

			//prix Unitaire

			column = table.AddColumn(Unit.FromCentimeter(1.5));
			column.Format.Alignment = ParagraphAlignment.Center;

			//total euros

			column = table.AddColumn(Unit.FromCentimeter(2));
			column.Format.Alignment = ParagraphAlignment.Center;

			//TVA

			column = table.AddColumn(Unit.FromCentimeter(1));
			column.Format.Alignment = ParagraphAlignment.Center;


			//en-tête du tableau
			Row row = table.AddRow();
			Cell cell = new Cell();

			for (int i = 0; i != 4; i++)
			{
				cell = row.Cells[i];
				cell.MergeRight = 4;
				cell.AddParagraph("Libellé");
				cell.Format.Alignment = ParagraphAlignment.Center;
				cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
				cell.Format.Font.Bold = true;
				cell.Shading.Color = Colors.Gray;
			}

			cell = row.Cells[5];
			cell.AddParagraph("U");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[6];
			cell.AddParagraph("Qté");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[7];
			cell.AddParagraph("Px unit. euros");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[8];
			cell.AddParagraph("Total euros");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[9];
			cell.AddParagraph("TVA");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			return table;
		}

		void DefineStyles()
		{
			Style style = this.document.Styles["Normal"];

			style.Font.Name = "Verdana";

			style = this.document.Styles[StyleNames.Header];

			style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

			style = this.document.Styles[StyleNames.Footer];

			style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

			style = this.document.Styles.AddStyle("Table", "Normal");
			style.Font.Name = "Verdana";
			style.Font.Name = "Arial";
			style.Font.Size = 9;
			style = this.document.Styles.AddStyle("Reference", "Normal");
			style.ParagraphFormat.SpaceBefore = "5mm";
			style.ParagraphFormat.SpaceAfter = "5mm";
			style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
		}

		void CreatePage()
		{
			int nbpage = 1;

			Section section = this.document.AddSection();

			string pathImage = Application.StartupPath + "/image/bmb_logo.png";

			MigraDoc.DocumentObjectModel.Shapes.Image image = section.Headers.Primary.AddImage(pathImage);
			image.Height = "4cm";
			image.LockAspectRatio = true;
			image.RelativeVertical = RelativeVertical.Line;
			image.RelativeHorizontal = RelativeHorizontal.Margin;
			image.Top = ShapePosition.Top;
			image.Left = "-1.5cm";
			image.WrapFormat.Style = WrapStyle.Through;



			// Create the text frame for the address

			Paragraph paragraph = new Paragraph();

			this.addressFrame = section.Headers.Primary.AddTextFrame();
			this.addressFrame.Height = "3.0cm";
			this.addressFrame.Width = "10cm";
			this.addressFrame.Left = "5cm";
			this.addressFrame.MarginLeft = Unit.FromCentimeter(3);
			this.addressFrame.Top = ShapePosition.Top;
			this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
			this.addressFrame.RelativeVertical = RelativeVertical.Line;

			paragraph = this.addressFrame.AddParagraph();
			paragraph.Format.SpaceBefore = "1cm";
			paragraph.AddFormattedText("Client :" + Environment.NewLine, TextFormat.Bold);
			paragraph.AddText(Facture.Client.Nom + Environment.NewLine + Facture.Client.Adresse);
			paragraph.Format.Font.Size = 10;
			paragraph.Format.Alignment = ParagraphAlignment.Left;

			paragraph = section.AddParagraph();
			paragraph.Style = "Reference";
			paragraph.Format.SpaceBefore = "4cm";

			if (Facture.IndiceFacture == "FC")
			{
				paragraph.AddFormattedText("FACTURE n°" + Facture.Numero, TextFormat.Bold);
				paragraph.AddSpace(55);
			}
			if (Facture.IndiceFacture == "FA")
			{
				paragraph.AddFormattedText("FACTURE D\'ACOMPTE n°" + Facture.Numero, TextFormat.Bold);
				paragraph.AddSpace(44);
			}
			if (Facture.IndiceFacture == "FL")
			{
				paragraph.AddFormattedText("BON DE LIVRAISON n°" + Facture.Numero, TextFormat.Bold);
				paragraph.AddSpace(42);
			}

			paragraph.AddText("du : ");
			paragraph.AddDateField(Facture.DateTime.ToString().Substring(0, 10));

			////element

			int nb = Facture.Elements.Count;

			Table table = this.CreatTable(section);

			double ttc = 0;
			if (nb != 0)
			{
				Row row = table.AddRow();
				Cell cell = new Cell();

				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
					cell.AddParagraph("Descriptions des Travaux :");
					cell.Format.Font.Bold = true;
					row.Borders.Bottom.Visible = false;
				}

				row = table.AddRow();

				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
					cell.AddParagraph(Facture.DescriptifTravaux);
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;
				}

				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				row.Borders.Top.Visible = false;
				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;

				}

				foreach (Element E in Facture.Elements)
				{

					//libellé
					row = table.AddRow();
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;
					for (int i = 0; i != 4; i++)
					{
						cell = row.Cells[i];
						cell.MergeRight = 4;
						cell.Format.Alignment = ParagraphAlignment.Left;
						cell.AddParagraph("- " + E.Libelle);
						if (E.IsOption == true)
						{
							cell.Shading.Color = Colors.LightGray;
						}
						cell.Borders.Top.Visible = false;
						cell.Borders.Bottom.Visible = false;

					}

					//unite
					cell = row.Cells[5];
					cell.AddParagraph(E.Unite.Libelle);
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//quantité
					cell = row.Cells[6];
					cell.AddParagraph(E.Quantite.ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//prix Unitaire
					cell = row.Cells[7];
					cell.AddParagraph(E.Prix_unit.ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//Total euros
					cell = row.Cells[8];
					decimal sum = E.Quantite * E.Prix_unit;
					sum = Math.Round(sum, 2);
					cell.AddParagraph(sum.ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//TVA
					cell = row.Cells[9];
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}
					if (Facture.TVA == "10%")
					{
						cell.AddParagraph("1");
					}
					else if (Facture.TVA == "20%")
					{
						cell.AddParagraph("1");
					}
					else if (Facture.TVA == "auto liquidation")
					{
						cell.AddParagraph("3");
					}

					row = table.AddRow();
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;
					for (int p = 0; p != 4; p++)
					{
						cell = row.Cells[p];
						cell.MergeRight = 4;

					}

					if (table.Rows.Count == 24 && E != Facture.Elements.Last())
					{
						row.Borders.Bottom.Visible = true;
						section.AddPageBreak();

						nbpage = nbpage + 1;

						paragraph = section.AddParagraph();
						paragraph.Format.SpaceBefore = "3cm";
						table = this.CreatTable(section);
						row = table.AddRow();
						row.Borders.Bottom.Visible = false;
						row.Borders.Top.Visible = false;
						for (int p = 0; p != 4; p++)
						{
							cell = row.Cells[p];
							cell.MergeRight = 4;
						}

					}

					if (E == Facture.Elements.Last())
					{
						int count = table.Rows.Count;
						if (count < 25)
						{
							for (int i = count; i <= 25; i++)
							{
								row = table.AddRow();
								row.Borders.Bottom.Visible = false;
								row.Borders.Top.Visible = false;
								for (int p = 0; p != 4; p++)
								{
									cell = row.Cells[p];
									cell.MergeRight = 4;

								}
							}
						}
						row.Borders.Bottom.Visible = true;
					}
				}




				//calcule

				double somme = 0;

				if (Facture.Voption == true)
				{
					foreach (Element E in Facture.Elements)
					{
						somme += Convert.ToDouble(E.Quantite * E.Prix_unit);

					}
				}
				else
				{
					foreach (Element E in Facture.Elements)
					{
						if (E.IsOption == false)
						{
							somme += Convert.ToDouble(E.Quantite * E.Prix_unit);
						}

					}
				}

				double tva = 0;
				if (Facture.TVA == "10%")
				{
					tva = (((double)10 / 100) * somme);
				}
				else if (Facture.TVA == "20%")
				{
					tva = (((double)20 / 100) * somme);
				}



				//1er ligne calcule
				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				cell = row.Cells[1];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("Taux", TextFormat.Bold);
				cell = row.Cells[2];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("Base Hors Taxes €", TextFormat.Bold);
				cell = row.Cells[3];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("TVA €", TextFormat.Bold);
				cell = row.Cells[4];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("TTC €", TextFormat.Bold);

				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}
				cell = row.Cells[5];
				somme = Math.Round(somme, 2);
				paragraph = cell.AddParagraph("Total Hors Taxes € : " + somme);
				paragraph.AddSpace(3);

				bool HaveOption = false;

				foreach(Element E in Facture.Elements)
				{
					if(E.IsOption == true)
					{
						HaveOption = true;
					}
				}

				if (HaveOption == true)
				{
					if (Facture.Voption == true)
					{
						FormattedText text = paragraph.AddFormattedText("Avec Option", TextFormat.Underline);
						text.Font.Color = Colors.Gray;
					}
					else
					{
						FormattedText text = paragraph.AddFormattedText("Hors Option", TextFormat.Underline);
						text.Font.Color = Colors.Gray;
					}
				}

				//2eme
				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				cell = row.Cells[0];
				if (Facture.TypeD.Libelle == "Professionnel")
				{
					cell.AddParagraph("2");
					cell = row.Cells[1];
					cell.AddParagraph("20%");

				}
				else if (Facture.TypeD.Libelle == "Particulier")
				{
					cell.AddParagraph("1");
					cell = row.Cells[1];
					cell.AddParagraph("10%");
				}
				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}
				cell = row.Cells[5];
				tva = Math.Round(tva, 2);
				cell.AddParagraph("Total TVA € : " + tva);



				//3eme
				row = table.AddRow();
				cell = row.Cells[0];
				if (Facture.TypeD.Libelle == "Professionnel")
				{
					cell.AddParagraph("3");
					cell = row.Cells[1];
					cell.AddParagraph("auto liqu.");
				}
				else if (Facture.TypeD.Libelle == "Particulier")
				{
					cell.AddParagraph("2");
					cell = row.Cells[1];
					cell.AddParagraph("20%");
				}
				cell = row.Cells[2];
				cell.AddParagraph(somme.ToString());

				cell = row.Cells[3];
				if (Facture.TVA != "auto liquidation")
					cell.AddParagraph(tva.ToString());

				cell = row.Cells[4];

				if (Facture.TVA == "10%")
				{
					ttc = somme + (((double)10 / 100) * somme);

				}
				else if (Facture.TVA == "20%")
				{
					ttc = somme + (((double)20 / 100) * somme);
				}
				else if (Facture.TVA == "auto liquidation")
				{
					ttc = somme;
				}

				ttc = Math.Round(ttc, 2);
				cell.AddParagraph(ttc.ToString());

				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}

				cell = row.Cells[5];
				if (Facture.TypeD.Libelle == "Professionnel")
					cell.AddParagraph("Total TTC € : " + ttc);

				if (Facture.TypeD.Libelle == "Particulier")
				{
					cell.AddParagraph("Total TTC € : " + ttc+ Environment.NewLine);

					if(Facture.NumeroDevis != null)
					{
						if (Facture.IndiceFacture == "FA")
						{
							paragraph = cell.AddParagraph();
							double calcule = ((double)40 / 100) *ttc;
							calcule = Math.Round(calcule,2);

							paragraph.AddFormattedText("Acompte sur le devis " + Facture.NumeroDevis + " TTC € : " + calcule.ToString() + Environment.NewLine, TextFormat.Bold);
							calcule = ((double)40 / 100) * tva;
							calcule = Math.Round(calcule,2);
							cell.AddParagraph("Dont TVA " + Facture.TVA + " € : " + calcule.ToString());
						}
						if (Facture.IndiceFacture == "FL")
						{
							paragraph = cell.AddParagraph();
							double calcule = ((double)60 / 100) * ttc;
							calcule = Math.Round(calcule,2);

							paragraph.AddFormattedText("Livraison sur le devis " + Facture.NumeroDevis + " TTC € : " + (calcule * ttc).ToString() + Environment.NewLine, TextFormat.Bold);

							calcule = ((double)60 / 100) * tva;
							calcule = Math.Round(calcule,2);

							cell.AddParagraph("Dont TVA " + Facture.TVA + " € : " + (calcule * tva).ToString());
						}
					}
					else
					{
						if (Facture.IndiceFacture == "FA")
						{
							cell.AddParagraph("Acompte TTC € : " + Math.Round((double)40 / 100 * ttc,2).ToString() + Environment.NewLine);
							cell.AddParagraph("Dont TVA " + Facture.TVA + "€ : " + Math.Round((double)40 / 100 * tva,2).ToString());
						}
						if (Facture.IndiceFacture == "FL")
						{
							cell.AddParagraph("Livraison TTC € : " + Math.Round((double)60 / 100 * ttc,2).ToString() + Environment.NewLine);
							cell.AddParagraph("Dont TVA " + Facture.TVA + "€ : " + Math.Round((double)60 / 100 * ttc, 2).ToString());
						}
					}

					
				}				
			}

			paragraph = section.Footers.Primary.AddParagraph();
			paragraph.AddText("S.A.S.BMB au capital de 10000€ - RCS Valenciennes 808 399 216 – NAF 4120A  "
								+ Environment.NewLine + "n° TVA FR71 808 399 216 – décennale n°1244000/001527144/0(SMA BTP)"
								+ Environment.NewLine + " 50 route de Marchipont, 59990 Rombies et Marchipont Tél: 0610491659 - Mail: bmbsasu @gmail.com");
			paragraph.Format.Font.Size = 6;
			paragraph.Format.Alignment = ParagraphAlignment.Center;

			paragraph = section.Footers.Primary.AddParagraph();
			paragraph.Format.SpaceBefore = "-0.5cm";
			paragraph.AddLineBreak();
			paragraph.AddLineBreak();
			paragraph.AddLineBreak();
			paragraph.Format.Font.Size = 8;
			paragraph.AddTab();
			paragraph.AddSpace(150);
			paragraph.Format.Alignment = ParagraphAlignment.Right;
			paragraph.AddPageField();
			paragraph.AddText("/" + nbpage);
			section.Footers.EvenPage.Add(paragraph.Clone());
		}
	}
}
