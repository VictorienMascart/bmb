﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevisFacile.MODEL.CLASSE
{
    class Avoir
    {
        //properties
        public int Id { get; set; }
        public float Montant { get; set; }
        public DateTime DateDebut { get; set; }
        public int Duree { get; set; }
        public bool Utilise { get; set; }

        public Client leClient { get; set; }

        //constructor
        public Avoir() { }
    
        public Avoir(int id, float montant, DateTime dateDebut, int duree, bool utilise, Client leclient)
        {
            Id = id;
            Montant = montant;
            DateDebut = dateDebut;
            Duree = duree;
            Utilise = utilise;

            leClient = leclient;
        }

        //methods

        public override string ToString()
        {
            return leClient.Nom + " : " + Montant;
        }

    }
}
