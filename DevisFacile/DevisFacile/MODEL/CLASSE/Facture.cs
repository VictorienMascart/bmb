﻿using DevisFacile.MODEL.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevisFacile.MODEL.CLASSE
{
	public class Facture
	{
		public int Id { get; set; }
		public string Numero { get; set; }
		public List<Element> Elements { get; set; }
		public Client Client { get; set; }
		public string TVA { get; set; }
		public DateTime DateTime { get; set; }
		public string DescriptifTravaux { get; set; }
		public TypeD TypeD { get; set; }
		public bool Voption { get; set; }
		public string IndiceFacture { get; set; }
		public string NumeroDevis { get; set; }

		public Facture() { }

		public Facture( int id, string num, DateTime dateTime, List<Element> elements, Client client, string tva, string Desc, TypeD typeD, bool opt, string indiceFacture, string numeroDevis)
		{
			Id = id;
			Numero = num;

			DateTime = dateTime;

			Elements = new List<Element>();
			Elements = elements;

			Client = new Client();
			Client = client;

			TVA = tva;

			DescriptifTravaux = Desc;

			TypeD = typeD;

			Voption = opt;

			IndiceFacture = indiceFacture;

			NumeroDevis = numeroDevis;

		}


		public override string ToString()
		{
			string date = DateTime.ToString();

			date = date.Substring(0, 10);


			ClientDAO clientDAO = new ClientDAO();

			this.Client = clientDAO.SelectChampsClient(Client);

			return this.Numero + " - " + Client.Nom + " - " + date; // client à ajouter
		}
	}
}
