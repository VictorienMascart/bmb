﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevisFacile.MODEL.CLASSE
{
	public class Unite
	{
		public int Id { get; set; }
		public string Libelle { get; set; }

		public Unite(int id)
		{
			Id = id;
		}

		public Unite()
		{
		}

		public Unite(int id, string libelle)
		{
			Libelle = libelle;
			Id = id;
		}

		public override string ToString()
		{
			return Libelle;
		}
	}
}