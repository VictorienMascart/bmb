﻿using PdfSharp.Charting;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using TabAlignment = MigraDoc.DocumentObjectModel.TabAlignment;
using MigraDoc.DocumentObjectModel.Shapes;
using DevisFacile.MODEL.DAO;

namespace DevisFacile.MODEL.CLASSE
{
	class CreationDevisPDF
	{
		public Devis Devis { get; set; }
		public Document document { get; set; }
		public TextFrame addressFrame { get; set; }

		public CreationDevisPDF(Devis devis)
		{
			if(devis.TypeD.Libelle == null)
			{
				DevisTypeDAO devisTypeDAO = new DevisTypeDAO();
				devis.TypeD = devisTypeDAO.SelectChampsType(devis.TypeD);
			}
			Devis = devis;
		}


		public Document CreateDocument()
		{
			this.document = new Document();
			if (Devis.Numero != null)
			{
				this.document.Info.Title = "Devis 2014-" + Devis.Numero.Substring(5) + ".pdf";
			}
			// Create a new MigraDoc document
			this.document.Info.Subject = "création d'un devis format pdf";
			this.document.Info.Author = "BMB";

			DefineStyles();

			CreatePage();

			return this.document;
		}

		private Table CreatTable(Section section)
		{
			Table table = section.AddTable();
			table.Style = "Table";
			table.Borders.Color = Colors.Black;
			table.Borders.Left.Width = 0.5;
			table.Borders.Right.Width = 0.5;
			table.Borders.Width = 0.25;

			//libelle
			Column column = table.AddColumn(Unit.FromCentimeter(0.5));

			column = table.AddColumn(Unit.FromCentimeter(1));
			column = table.AddColumn(Unit.FromCentimeter(3.5));
			column = table.AddColumn(Unit.FromCentimeter(3));
			column = table.AddColumn(Unit.FromCentimeter(3));


			//unite
			column = table.AddColumn(Unit.FromCentimeter(1));
			column.Format.Alignment = ParagraphAlignment.Center;

			//quantité
			column = table.AddColumn(Unit.FromCentimeter(1));
			column.Format.Alignment = ParagraphAlignment.Center;

			//prix Unitaire

			column = table.AddColumn(Unit.FromCentimeter(1.5));
			column.Format.Alignment = ParagraphAlignment.Center;

			//total euros

			column = table.AddColumn(Unit.FromCentimeter(1.5));
			column.Format.Alignment = ParagraphAlignment.Center;

			//TVA

			column = table.AddColumn(Unit.FromCentimeter(1.5));
			column.Format.Alignment = ParagraphAlignment.Center;


			//en-tête du tableau
			Row row = table.AddRow();
			Cell cell = new Cell();

			for (int i = 0; i != 4; i++)
			{
				cell = row.Cells[i];
				cell.MergeRight = 4;
				cell.AddParagraph("Libellé");
				cell.Format.Alignment = ParagraphAlignment.Center;
				cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
				cell.Format.Font.Bold = true;
				cell.Shading.Color = Colors.Gray;
			}

			cell = row.Cells[5];
			cell.AddParagraph("U");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[6];
			cell.AddParagraph("Qté");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[7];
			cell.AddParagraph("Px unit. euros");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[8];
			cell.AddParagraph("Total euros");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			cell = row.Cells[9];
			cell.AddParagraph("TVA");
			cell.Format.Font.Bold = true;
			cell.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
			cell.Shading.Color = Colors.Gray;

			return table;
		}

		void DefineStyles()
		{
			Style style = this.document.Styles["Normal"];

			style.Font.Name = "Verdana";

			style = this.document.Styles[StyleNames.Header];

			style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

			style = this.document.Styles[StyleNames.Footer];

			style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

			style = this.document.Styles.AddStyle("Table", "Normal");
			style.Font.Name = "Verdana";
			style.Font.Name = "Arial";
			style.Font.Size = 9;
			style = this.document.Styles.AddStyle("Reference", "Normal");
			style.ParagraphFormat.SpaceBefore = "5mm";
			style.ParagraphFormat.SpaceAfter = "5mm";
			style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
		}

		void  CreatePage()
		{
			int nbpage = 1;

			Section section = this.document.AddSection();

			string pathImage = Application.StartupPath + "/image/bmb_logo.png";

			MigraDoc.DocumentObjectModel.Shapes.Image image = section.Headers.Primary.AddImage(pathImage);
			image.Height = "4cm";
			image.LockAspectRatio = true;
			image.RelativeVertical = RelativeVertical.Line;
			image.RelativeHorizontal = RelativeHorizontal.Margin;
			image.Top = ShapePosition.Top;
			image.Left = "-1.5cm";
			image.WrapFormat.Style = WrapStyle.Through;



			// Create the text frame for the address

			Paragraph paragraph = new Paragraph();

			this.addressFrame = section.Headers.Primary.AddTextFrame();
			this.addressFrame.Height = "3.0cm";
			this.addressFrame.Width = "10cm";
			this.addressFrame.Left = "5cm";
			this.addressFrame.MarginLeft = Unit.FromCentimeter(3);
			this.addressFrame.Top = ShapePosition.Top;
			this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
			this.addressFrame.RelativeVertical = RelativeVertical.Line;

			paragraph = this.addressFrame.AddParagraph();
			paragraph.Format.SpaceBefore = "1cm";
			paragraph.AddFormattedText("Client :" + Environment.NewLine, TextFormat.Bold);
			paragraph.AddText(Devis.Client.Nom + Environment.NewLine + Devis.Client.Adresse);
			paragraph.Format.Font.Size = 10;
			paragraph.Format.Alignment = ParagraphAlignment.Left;

			paragraph = section.AddParagraph();
			paragraph.Format.SpaceBefore = "2cm";
			paragraph.Style = "Reference";
			paragraph.AddSpace(20);
			paragraph.AddFormattedText("DEVIS n°"+Devis.Numero, TextFormat.Bold);
			paragraph.AddSpace(45);
			paragraph.AddText("du : ");
			paragraph.AddDateField(Devis.Date.ToString().Substring(0, 10));

			////element

			int nb = Devis.Elements.Count;

			Table table = this.CreatTable(section);
            

			double ttc = 0;
			if (nb != 0)
			{
				Row row = table.AddRow();
				Cell cell = new Cell();

				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
					cell.AddParagraph("Descriptions des Travaux :");
					cell.Format.Font.Bold = true;
					row.Borders.Bottom.Visible = false;
				}

				row = table.AddRow();

				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
					cell.AddParagraph(Devis.DescriptionTravaux);
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;
				}

				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				row.Borders.Top.Visible = false;
				for (int i = 0; i != 4; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;

				}

                int count = 0;


                foreach (Element E in Devis.Elements)
				{

					//if (table.Rows.Count > 24)
					//{
     //                   row.Borders.Bottom.Visible = true;
					//	section.AddPageBreak();

					//	nbpage = nbpage + 1;

					//	paragraph = section.AddParagraph();
					//	paragraph.Format.SpaceBefore = "3cm";
					//	table = this.CreatTable(section);
					//	row = table.AddRow();
					//	row.Borders.Bottom.Visible = false;
					//	row.Borders.Top.Visible = false;
					//	for (int p = 0; p != 4; p++)
					//	{
					//		cell = row.Cells[p];
					//		cell.MergeRight = 4;
					//	}
					//}

					//libellé
					row = table.AddRow();
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;

					for (int i = 0; i != 4; i++)
					{
						cell = row.Cells[i];
						cell.MergeRight = 4;
						cell.Format.Alignment = ParagraphAlignment.Left;
						cell.AddParagraph("- " + E.Libelle);
						if (E.IsOption == true)
						{
							cell.Shading.Color = Colors.LightGray;
						}
						cell.Borders.Top.Visible = false;
						cell.Borders.Bottom.Visible = false;
                    }
	

					//unite
					cell = row.Cells[5];
					cell.AddParagraph(E.Unite.Libelle);
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//quantité
					cell = row.Cells[6];
					cell.AddParagraph(E.Quantite.ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//prix Unitaire
					cell = row.Cells[7];
					cell.AddParagraph(E.Prix_unit.ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//Total euros
					cell = row.Cells[8];
					cell.AddParagraph(Math.Round(E.Quantite * E.Prix_unit, 2).ToString());
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;
					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}

					//TVA
					cell = row.Cells[9];
					cell.Borders.Top.Visible = false;
					cell.Borders.Bottom.Visible = false;

					if (E.IsOption == true)
					{
						cell.Shading.Color = Colors.LightGray;
					}
					if (Devis.Tva == "10%")
					{
						cell.AddParagraph("1");
					}
					else if (Devis.Tva == "20%")
					{
						cell.AddParagraph("1");
					}
					else if (Devis.Tva == "auto liquidation")
					{
						cell.AddParagraph("3");
					}

                    
					row = table.AddRow();
					row.Borders.Bottom.Visible = false;
					row.Borders.Top.Visible = false;
					for (int p = 0; p != 4; p++)
					{
						cell = row.Cells[p];
						cell.MergeRight = 4;
					}

				}

				count = table.Rows.Count;
				if (count < 20)
				{
					for (int i = count; i <= 20; i++)
					{
						row = table.AddRow();
						row.Borders.Bottom.Visible = false;
						row.Borders.Top.Visible = false;
						for (int p = 0; p != 4; p++)
						{
							cell = row.Cells[p];
							cell.MergeRight = 4;

						}
					}
				}
				row.Borders.Bottom.Visible = true;




				//calcule

				double somme = 0;

				if (Devis.ValidOption == true)
				{
					foreach (Element E in Devis.Elements)
					{
						somme += Convert.ToDouble(E.Quantite * E.Prix_unit);

					}
				}
				else
				{
					foreach (Element E in Devis.Elements)
					{
						if (E.IsOption == false)
						{
							somme += Convert.ToDouble(E.Quantite * E.Prix_unit);
						}

					}
				}

				double tva = 0;
				if (Devis.Tva == "10%")
				{
					tva = (((double)10 / 100) * somme);


				}
				else if (Devis.Tva == "20%")
				{
					tva = (((double)20 / 100) * somme);
				}



				//1er ligne calcule
				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				cell = row.Cells[1];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("Taux", TextFormat.Bold);
				cell = row.Cells[2];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("Base Hors Taxes €", TextFormat.Bold);
				cell = row.Cells[3];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("TVA €", TextFormat.Bold);
				cell = row.Cells[4];
				paragraph = cell.AddParagraph();
				paragraph.AddFormattedText("TTC €", TextFormat.Bold);

				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}
				cell = row.Cells[5];
				paragraph = cell.AddParagraph("Total Hors Taxes € : " + Math.Round(somme, 2));
				paragraph.AddSpace(3);


				bool HaveOption = false;

				foreach (Element E in Devis.Elements)
				{
					if (E.IsOption == true)
					{
						HaveOption = true;
					}
				}

				if (HaveOption == true)
				{
					if (Devis.ValidOption == true)
					{
						FormattedText text = paragraph.AddFormattedText("Avec Option", TextFormat.Underline);
						text.Font.Color = Colors.Gray;
					}
					else
					{
						FormattedText text = paragraph.AddFormattedText("Hors Option", TextFormat.Underline);
						text.Font.Color = Colors.Gray;
					}
				}



				//2eme
				row = table.AddRow();
				row.Borders.Bottom.Visible = false;
				cell = row.Cells[0];
				if (Devis.TypeD.Libelle == "Professionnel")
				{
					cell.AddParagraph("2");
					cell = row.Cells[1];
					cell.AddParagraph("20%");

				}
				else if (Devis.TypeD.Libelle == "Particulier")
				{
					cell.AddParagraph("1");
					cell = row.Cells[1];
					cell.AddParagraph("10%");
				}
				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}
				cell = row.Cells[5];
				cell.AddParagraph("Total TVA € : " + Math.Round(tva, 2));



				//3eme
				row = table.AddRow();
				cell = row.Cells[0];
				if (Devis.TypeD.Libelle == "Professionnel")
				{
					cell.AddParagraph("3");
					cell = row.Cells[1];
					cell.AddParagraph("auto liqu.");
				}
				else if (Devis.TypeD.Libelle == "Particulier")
				{
					cell.AddParagraph("2");
					cell = row.Cells[1];
					cell.AddParagraph("20%");
				}
				cell = row.Cells[2];
				cell.AddParagraph(Math.Round(somme, 2).ToString());

				cell = row.Cells[3];
				if (Devis.Tva != "auto liquidation")
					cell.AddParagraph(Math.Round(tva, 2).ToString());

				cell = row.Cells[4];

				if (Devis.Tva == "10%")
				{
					ttc = somme + (((double)10 / 100) * somme);

				}
				else if (Devis.Tva == "20%")
				{
					ttc = somme + (((double)20 / 100) * somme);
				}
				else if (Devis.Tva == "auto liquidation")
				{
					ttc = somme;
				}

				cell.AddParagraph(Math.Round(ttc, 2).ToString());

				for (int i = 5; i < 9; i++)
				{
					cell = row.Cells[i];
					cell.MergeRight = 4;
					cell.Format.Alignment = ParagraphAlignment.Left;
				}

				cell = row.Cells[5];
				cell.AddParagraph("Total TTC € : " + Math.Round(ttc, 2));

			}


			paragraph = section.AddParagraph();
			paragraph.Format.Font.Size = 7.5;
			if(Devis.TypeD.Libelle == "Professionnel")
			{
				paragraph.AddFormattedText("MARCHÉ - RÉGLEMENTS"+ Environment.NewLine, TextFormat.Bold | TextFormat.Underline);
				paragraph.AddText(Devis.TypeD.Description);
			}
			else if(Devis.TypeD.Libelle == "Particulier")
			{
				paragraph = section.AddParagraph();
				paragraph.Format.Font.Size = 7.5;
				paragraph.AddFormattedText("MODE DE RÉGLEMENTS €:"+ Environment.NewLine, TextFormat.Bold | TextFormat.Underline);
				paragraph.AddText("40% à la commande, soit :"+ Math.Round((double)40 / 100 * ttc,2).ToString() + Environment.NewLine); 
				paragraph.AddText("60% à la livraison, soit :"+ Math.Round((double)60 / 100 * ttc,2).ToString() + Environment.NewLine);
				paragraph.AddFormattedText("RÉGLEMENTS" + Environment.NewLine, TextFormat.Bold | TextFormat.Underline);
				paragraph.AddText(Devis.TypeD.Description);
			}

			paragraph = section.AddParagraph();
			paragraph.Format.SpaceBefore = "0.5cm";
			paragraph.Format.Font.Size = 7;
			paragraph.AddFormattedText("TRAVAUX NON COMPRIS"+Environment.NewLine, TextFormat.Bold | TextFormat.Underline);
			paragraph.AddText("Ce qui ne figure pas dans notre devis.");

			paragraph = section.AddParagraph();
			paragraph.Format.SpaceBefore = "0.5cm";
			paragraph.Format.Font.Size = 7;
			paragraph.AddFormattedText("TRAVAUX COMPRIS" + Environment.NewLine, TextFormat.Bold | TextFormat.Underline);
			paragraph.AddText("Lu et approuvé \"bon pour commande\" suivant les conditions et descriptions du devis"+Environment.NewLine);
			paragraph.AddText("Signature du client :");


			this.addressFrame = section.AddTextFrame();
			this.addressFrame.Height = "5cm";
			this.addressFrame.Width = "20cm";
			this.addressFrame.Left = "10cm";
			this.addressFrame.MarginLeft = Unit.FromCentimeter(3);
			this.addressFrame.MarginBottom = Unit.FromCentimeter(1);
			this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
			this.addressFrame.RelativeVertical = RelativeVertical.Line;
			paragraph = addressFrame.AddParagraph();
			paragraph.Format.Font.Size = 8;
			paragraph.AddText("Validité de l'offre :30 jours "+Environment.NewLine+" Fait en trois exemplaires" + Environment.NewLine);
			paragraph.AddFormattedText("Pour l\'entreprise :", TextFormat.Underline);
			paragraph.Format.SpaceBefore = "-0.5cm";

			string pathsignature = Application.StartupPath + "/image/signature.jpg";

			MigraDoc.DocumentObjectModel.Shapes.Image imageSignature = addressFrame.AddImage(pathsignature);
			image.Height = "2.5cm";
			image.LockAspectRatio = true;




			paragraph = section.Footers.Primary.AddParagraph();
			paragraph.AddText("S.A.S.BMB au capital de 10000€ - RCS Valenciennes 808 399 216 – NAF 4120A  "
								+ Environment.NewLine + "n° TVA FR71 808 399 216 – décennale n°1244000/001527144/0(SMA BTP)"
								+ Environment.NewLine + " 50 route de Marchipont, 59990 Rombies et Marchipont Tél: 0610491659 - Mail: bmbsasu @gmail.com");

			paragraph.Format.Font.Size = 6;
			paragraph.Format.Alignment = ParagraphAlignment.Center;

			paragraph = section.Footers.Primary.AddParagraph();
			paragraph.Format.SpaceBefore = "-0.5cm";
			paragraph.AddLineBreak();
			paragraph.AddLineBreak();
			paragraph.AddLineBreak();
			paragraph.Format.Font.Size = 8;
			paragraph.AddTab();
			paragraph.AddSpace(150);
			paragraph.Format.Alignment = ParagraphAlignment.Right;
			paragraph.AddPageField();
			paragraph.AddText("/" + nbpage);
			section.Footers.EvenPage.Add(paragraph.Clone());
		}
	}
}

