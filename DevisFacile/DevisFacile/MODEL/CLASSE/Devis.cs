﻿using DevisFacile.MODEL.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevisFacile.MODEL.CLASSE
{
	public class Devis
	{
		//properties
		public int Id { get; set; }
		public string Numero { get; set; }

		public DateTime Date { get; set; }
		public List<Element> Elements { get; set; }
		public TypeD TypeD { get; set; }
		public Client Client { get; set; }

		public string Tva { get; set; }

		public string DescriptionTravaux { get; set; }

		public bool ValidOption {get;set;}
		

		//constructor
		public Devis(int id,string numero, List<Element> elements, TypeD typeD, Client client)
		{
			Id = id;
			Date = DateTime.Now;
			Numero = numero;

			Elements = elements;

			TypeD = new TypeD();
			TypeD = typeD;

			Client = client;
		}

		public Devis(int id, string numero, DateTime date, List<Element> elements, TypeD typeD, Client client, string descriptionTravaux, bool validOption)
		{
			Id = id;
			Numero = numero;
			Date = date;

			Elements = elements;

			TypeD = new TypeD();
			TypeD = typeD;

			Client = client;
			DescriptionTravaux = descriptionTravaux;

			ValidOption = validOption;
		}

		public Devis(string numero, DateTime date, List<Element> elements, TypeD typeD, Client client, string descriptionTravaux, bool validOption, string TVA)
		{
			Numero = numero;

			Date = date;

			Elements = elements;

			TypeD = new TypeD();
			TypeD = typeD;

			Client = client;
			DescriptionTravaux = descriptionTravaux;

			ValidOption = validOption;

			Tva = TVA;
		}

		public Devis(string numero, DateTime date)
		{
			Numero = numero;
			Date = date;
		}

		public Devis()
		{
		}

		public override string ToString()
		{
			string date = Date.ToString();

			date = date.Substring(0, 10);


			ClientDAO clientDAO = new ClientDAO();

			this.Client = clientDAO.SelectChampsClient(Client);

			return this.Numero +" - "+ Client.Nom +" - "+date; // client à ajouter
		}

	}
}
