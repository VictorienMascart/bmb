﻿namespace DevisFacile.MODEL.CLASSE
{
	public class Client
	{
		public int Id { get; set; }
		public string Nom { get; set; }
		public string Adresse { get; set; }

		public Client(int id, string nom, string adresse)
		{
			Id = id;
			Nom = nom;
			Adresse = adresse;
		}

		public Client(string nom, string adresse)
		{
			Nom = nom;
			Adresse = adresse;
		}

		public Client() { }

		public override string ToString()
		{
			return Nom + " /adresse :" + Adresse;
		}

	}
}